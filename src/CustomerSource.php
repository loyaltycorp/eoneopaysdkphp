<?php

namespace EoneoPay;

class CustomerSource extends Resource
{
    public $customer_id;

    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getRequiredProperties()
    {
        return [];
    }

    static protected function getEndPoint($instance = null)
    {
        if (is_null($instance)) {
            return "customers/0/paymentSources";
        } else {
            return "customers/$instance->customer_id/paymentSources";
        }
    }

    static protected function getObjectFromResponse($response, $instance = null, $root = false)
    {
        $result = false;

        $object = json_decode($response->getBody());
        if ($object->type == "credit_card") {
            $result = CreditCard::getObjectFromResponse($response, $instance, true);
        } else if ($object->type == "bank_account") {
            $result = BankAccount::getObjectFromResponse($response, $instance, true);
        }

        return $result;
    }

    /*public function setDefault()
    {
        return false;
    }

    public function isDefault()
    {
        return $this->default;
    }*/
}
