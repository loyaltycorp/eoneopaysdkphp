<?php

namespace EoneoPay;

/**
 * Base class for all API resources.
 *
 * Implementing classes must provide:
 *
 * 1. getIdProperty() - e.g. 'id'
 * 2. getEndpoint() - e.g. 'customers'
 * 3. getRequiredProperties() - e.g. ['email']
 *
 * In addition implementing classes may override getObjectFromResponse() to
 * parse API responses correctly for special cases. Most API responses
 * format the data in a standard way allowing this base class to make
 * assumptions about parsing objects from teh response.
 *
 * Where the response data format varies from the standard the implementing class
 * must override getObjetFromResponse to ensure class are instantiated correctly.
 */
abstract class Resource
{
    /**
     * Get the ID property name for this resource.
     */
    static abstract protected function getIdProperty();

    /**
     * Get the endpoint for this resource
     */
    static abstract protected function getEndpoint($instance = null);

    /**
     * Get an array of required properties for this resource.
     *
     * New resources cannot be created unless values are provided for these properties.
     */
    static abstract protected function getRequiredProperties();

    /**
     * Get an instance of the resource from the HTTP response body.
     *
     * This default method assumes the resource is represented in the response by an object 
     * named for the class name. If $root is true then the entire body is assumed to represent
     * the resource.
     *
     * Some resources need to handle the response body differently so those classes override
     * this method.
     *
     */
    static protected function getObjectFromResponse($response, $instance = null, $root = false)
    {
        if ($root) {
            $object = json_decode($response->getBody());
        } else {
            $className = static::getClassName();
            $object = json_decode($response->getBody())->$className;
        }

        return static::getObjectForStdClass($object);
    }

    static private function getObjectForStdClass($object)
    {
        $result = new static();
        foreach (get_object_vars($object) as $name => $value) {
            $assignedValue = static::assignValue($value);
            $result->$name = $assignedValue;
        }

        return $result;
    } 

    static protected function getListFromResponse($response)
    {
        $result = [];

        $list = json_decode($response->getBody())->list;
        foreach ($list as $item) {
            $result[] = static::getObjectForStdClass($item);
        }

        return $result;
    }

    static protected function getClassName()
    {
        $fullClassName = get_called_class();
        $parts = explode('\\', $fullClassName);
        $className = strtolower($parts[sizeof($parts) - 1]);
        return $className;
    }

    static private function assignValue($value)
    {
        if (is_object($value)) {
            return static::getObjectForResponseValue($value);
        } else if (is_array($value)) {
            $arrayValue = [];
            foreach ($value as $item) {
                $arrayValue[] = static::assignValue($item);
            }
            return $arrayValue;
        }

        return $value;
    }

    static protected function getObjectForResponseValue($responseValue)
    {
        return $responseValue;
    }

    /**
     * Get the ID for this resource.
     *
     * The ID is used in calls to GET, DELETE and UPDATE.
     */
    public function getId()
    {
        $idProperty = static::getIdProperty();
        if (property_exists($this, $idProperty)) {
            return $this->$idProperty;
        } else {
            return null;
        }
    }

    /**
     * Client code saves resources using this method.
     *
     * Any resource that is already saved or has been retrieved through the
     * API is expected to have the ID attribute set (not the case before
     * an item is saved).
     *
     * This method checks for an existing ID and calls create() where no ID
     * is found. Where an ID does exist update() is called.
     */
    public function save()
    {
        if (is_null($this->getId())) {
            return $this->create();
        } else {
            return $this->update();
        }
    }

    /**
     * Create a new instance of this resource.
     */
    protected function create()
    {
        $properties = get_object_vars($this);
        //Verify required properties are set here
        $response = static::makeRequest(EoneoPay::POST, static::getEndPoint($this), $properties);
        if ($response->getStatusCode() == 200) {
            $object = static::getObjectFromResponse($response, $this);
            $this->{static::getIdProperty()} = $object->{static::getIdProperty()};
            return $object;
        }

        return null;
    }

    /**
     * Retrieve a specific instance of this resource.
     */
    static public function retrieve($id = 'self')
    {
        $response = static::makeRequest(EoneoPay::GET, static::getEndPoint() . "/" . $id);
        if ($response->getStatusCode() == 200) {
            return static::getObjectFromResponse($response, null, true);
        }

        return null;
    }

    /**
     * Delete an instance of this resource.
     */
    public function delete($returnResponse = false)
    {
        $response = static::makeRequest(EoneoPay::DELETE, static::getEndPoint($this) . "/" . $this->getId());
        if ($returnResponse && $response->getStatusCode() == 200) {
            return static::getObjectFromResponse($response, null, true);
        }

        return $response->getStatusCode() == 200;
    }

    /**
     * Update the resource with the given properties.
     */
    protected function update()
    {
        $properties = get_object_vars($this);
        $response = static::makeRequest(EoneoPay::PUT, static::getEndPoint($this) . "/" . $this->getId(), $properties);
        if ($response->getStatusCode() == 200) {
            $object = static::getObjectFromResponse($response, $this);
            $this->{static::getIdProperty()} = $object->{static::getIdProperty()};
            return $object;
        }
        return null;
    }

    /**
     * Get a list of resource instances.
     */
    static public function all($filter = [], $limit = 100, $offset = 0, $includes = [])
    {
        $query = ['offset' => $offset, 'limit' => $limit];
        if (!empty($filter)) {
            $query["filter"] = $filter;
        }
        if (!empty($includes)) {
            $query["includes"] = implode(',', $includes);
        }
        $response = static::makerequest(EoneoPay::GET, static::getEndPoint() . "?" . http_build_query($query));
        if ($response->getStatusCode() == 200) {
            return static::getListFromResponse($response);
        }
    }

    static protected function makeRequest($method, $endPoint, $data = null)
    {
        return EoneoPay::makeRequest($method, $endPoint, $data);
    }
}
