<?php

namespace EoneoPay;

/**
 * Class to create and manage customers.
 */
class Plan extends Resource
{
    const INTERVAL_DAY = 'day';
    const INTERVAL_WEEK = 'week';
    const INTERVAL_MONTH = 'month';
    const INTERVAL_YEAR = 'year';

    public $currency = 'AUD';
    public $fees = [];

    static function __init__()
    {
        EoneoPay::registerEoneoException('400', '4000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('404', '4100', 'EoneoPay\Exception\ResourceNotFoundException');
    }

    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        return "plans";
    }

    static protected function getRequiredProperties()
    {
        return ['name', 'amount', 'interval'];
    }

    public function addFee(PlanFee $fee)
    {
        $this->fees[] = $fee;
    }
}

Plan::__init__();
