<?php

namespace EoneoPay;

/**
 * Class to create test batch payments.
 */
class TestBatchPayment extends Resource
{
    const BATCH_TYPE_BPAY = 'bpay';
    const BATCH_TYPE_ALLOCATED_EFT = 'eft';
    const BATCH_TYPE_AUSPOST = 'auspost';

    static function __init__()
    {
        //Customer exceptions
        EoneoPay::registerEoneoException('400', '90000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('403', '90001', 'EoneoPay\Exception\TestingInProductionException');
        EoneoPay::registerEoneoException('400', '90002', 'EoneoPay\Exception\InvalidPaymentTypeException');
        EoneoPay::registerEoneoException('404', '90101', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('404', '90103', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('404', '90103', 'EoneoPay\Exception\ResourceNotFoundException');
    }

    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        return "testBatchPayments";
    }

    static protected function getRequiredProperties()
    {
        return ['type', 'amount'];
    }

    public function submit()
    {
        return parent::save();
    }
}

TestBatchPayment::__init__();
