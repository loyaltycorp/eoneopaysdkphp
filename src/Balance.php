<?php

namespace EoneoPay;

/**
 * Class to retrieve account balances
 */
class Balance extends Resource
{
    static function __init__()
    {
        EoneoPay::registerEoneoException('404', '13101', 'EoneoPay\Exception\ResourceNotFoundException');
    }

    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        return "balance";
    }

    static protected function getRequiredProperties()
    {
        return [];
    }

    /**
     * Retrieve a specific instance of this resource.
     */
    static public function retrieve($id = null)
    {
        $response = EoneoPay::makeRequest(EoneoPay::GET, static::getEndPoint());
        if ($response->getStatusCode() == 200) {
            return static::getObjectFromResponse($response, null, true);
        }

        return $resource;
    }

    /**
     * Retrieve balance history
     */
    static public function history($filter = [])
    {
        $args = '';
        foreach ($filter as $name => $value) {
            $args .= urlencode($name) . "=" . urlencode($value) . "&";
        }
        $response = EoneoPay::makeRequest(EoneoPay::GET, static::getEndPoint() . "/history?" . $args);
        if ($response->getStatusCode() == 200) {
            return static::getObjectFromResponse($response, null, true);
        }

        return $resource;
    }

    static protected function getObjectFromResponse($response, $instance = null, $root = false)
    {
        $object = json_decode($response->getBody());

        $result = new static();
        foreach (get_object_vars($object) as $name => $value) {
            $result->$name = $value;
        }

        return $result;
    } 
}

Balance::__init__();
