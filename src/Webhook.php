<?php

namespace EoneoPay;

/**
 * Class to create and manage customers.
 */
class Webhook extends Resource
{
    static function __init__()
    {
        EoneoPay::registerEoneoException('404', '16000', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('400', '16001', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('400', '16002', 'EoneoPay\Exception\EoneoValidationException');
    }

    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        return "webhooks";
    }

    static protected function getRequiredProperties()
    {
        return ['name', 'event_name', 'target_url', 'merchant_id'];
    }
}

Webhook::__init__();
