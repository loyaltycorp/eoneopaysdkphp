<?php

namespace EoneoPay;

/**
 * Class to create and manage customers.
 */
class Charge extends Resource
{
    const PRECEDENCE_FIRST = 'first';
    const PRECEDENCE_SHARED = 'shared';
    const PRECEDENCE_LAST = 'last';

    static function __init__()
    {
        EoneoPay::registerEoneoException('400', '5000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('400', '5001', 'EoneoPay\Exception\InvalidAmountException');
        EoneoPay::registerEoneoException('404', '5100', 'EoneoPay\Exception\ResourceNotFoundException');
    }

    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        return "charges";
    }

    static protected function getRequiredProperties()
    {
        return ['name', 'amount'];
    }
}

Charge::__init__();
