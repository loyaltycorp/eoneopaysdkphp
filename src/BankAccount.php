<?php

namespace EoneoPay;

class BankAccount extends CustomerSource
{
    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        if (is_null($instance)) {
            return "customers/0/bankAccounts";
        } else {
            return "customers/$instance->customer_id/bankAccounts";
        }
    }

    static protected function getRequiredProperties()
    {
        return ['name', 'number', 'bsb'];
    }

    static protected function getObjectFromResponse($response, $instance = null, $root = false)
    {
        if ($root) {
            $object = json_decode($response->getBody());
        } else {
            if (!is_null($instance)) {
                $credit_card = json_decode($response->getBody())->bank_account;
                $object = new CreditCard;
                foreach (get_object_vars($credit_card) as $name => $value) {
                    $object->$name = $value;
                }
            } else {
                $object = json_decode($response->getBody())->bank_account;
            }
        }

        $result = new static();
        foreach (get_object_vars($object) as $name => $value) {
            $result->$name = $value;
        }

        return $result;
    }

    public function setDefault()
    {
        $response = static::makeRequest(EoneoPay::PUT, static::getEndPoint($this) . "/$this->id");
        if ($response->getStatusCode() == 200) {
            $object = static::getObjectFromResponse($response);
            $this->{static::getIdProperty()} = $object->{static::getIdProperty()};
            return $object;
        }

        return false;
    }

    public function isDefault()
    {
        return $this->default;
    }
}
