<?php

namespace EoneoPay;

class Subscription extends Resource
{
    static function __init__()
    {
        //Customer exceptions
        EoneoPay::registerEoneoException('400', '7000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('409', '7001', 'EoneoPay\Exception\DuplicateBpayReferenceException');
        EoneoPay::registerEoneoException('400', '7002', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('400', '7003', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('400', '7009', 'EoneoPay\Exception\InvalidPaymentSourceException');
        EoneoPay::registerEoneoException('404', '7100', 'EoneoPay\Exception\ResourceNotFoundException');
    }
        
    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        if (is_null($instance)) {
            return "customers/0/subscriptions";
        } else {
            return "customers/$instance->customer_id/subscriptions";
        }
    }

    static protected function getRequiredProperties()
    {
        return ['plan'];
    }

    static public function getList($customer_id, $filter = null)
    {
        $instance = new Subscription;
        $instance->customer_id = $customer_id;
        $response = static::makeRequest(EoneoPay::GET, static::getEndPoint($instance) . (is_null($filter) ?  "" : "?" . http_build_query(['filter' => $filter])));
        if ($response->getStatusCode() == 200) {
            return static::getListFromResponse($response);
        }
    }

    public function getPayments($filter = [], $limit = 100, $offset = 0)
    {
        if (empty($filter)) {
            $filter = [];
        }
        $filter = array_merge($filter, ["reference_number" => $this->id]);
        $payments = Payment::all($filter, $limit, $offset);

        if (property_exists($this, "bpay_reference_number")) {
            $filter = array_merge($filter, ["reference_number" => $this->bpay_reference_number]);
            $payments = array_merge($payments, Payment::all($filter, $limit, $offset));
        }

        return $payments;
    }

    public function addCharge(Charge $charge)
    {
        $idProperty = static::getIdProperty();
        $id = $this->$idProperty;
        $response = static::makeRequest(EoneoPay::PUT, static::getEndPoint($this) . "/" . $id . "/charges/" . $charge->id);
        if ($response->getStatusCode() == 200) {
            return static::getObjectFromResponse($response);
        }
    }

    public function getStatement($date = false)
    {
        if ($date === false) {
            $date = date("Y-m-d");
        }

        $idProperty = static::getIdProperty();
        $id = $this->$idProperty;
        $response = static::makeRequest(EoneoPay::GET, static::getEndPoint($this) . "/" . $id . "/statement?" . $date);
        if ($response->getStatusCode() == 200) {
            return json_decode($response->getBody());
        }
    }

    public function cancel()
    {
        return parent::delete();
    }

    static public function getOverdue($atDate = false, $limit = 100, $offset = 0)
    {
        if (!$atDate) {
            $atDate = date("Y-m-d");
        }

        $query = ['offset' => $offset, 'limit' => $limit, 'atDate' => $atDate];

        $response = static::makeRequest(EoneoPay::GET, "overdueSubscriptions?" . http_build_query($query));
        if ($response->getStatusCode() == 200) {
            return static::getListFromResponse($response);
        }
    }
}

Subscription::__init__();
