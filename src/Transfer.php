<?php

namespace EoneoPay;

/**
 * Class to create and manage customers.
 */
class Transfer extends Resource
{
    static function __init__()
    {
        //Customer exceptions
        EoneoPay::registerEoneoException('400', '8000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('403', '8001', 'EoneoPay\Exception\AvailableBalanceExceededException');
        EoneoPay::registerEoneoException('400', '8002', 'EoneoPay\Exception\TransferAmountMismatchException');
        EoneoPay::registerEoneoException('404', '8100', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('404', '8101', 'EoneoPay\Exception\ResourceNotFoundException');
    }

    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        return "transfers";
    }

    static protected function getRequiredProperties()
    {
        return [];
    }

    public function submit()
    {
        return parent::save();
    }
}
