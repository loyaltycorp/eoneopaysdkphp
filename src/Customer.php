<?php

namespace EoneoPay;

/**
 * Class to create and manage customers.
 */
class Customer extends Resource
{
    static function __init__()
    {
        //Customer exceptions
        EoneoPay::registerEoneoException('400', '2000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('409', '2001', 'EoneoPay\Exception\DuplicateCustomerException');
        EoneoPay::registerEoneoException('500', '2010', 'EoneoPay\Exception\TokenisationFailedException');
        EoneoPay::registerEoneoException('404', '2100', 'EoneoPay\Exception\ResourceNotFoundException');
        
        //CardHolder exceptions
        EoneoPay::registerEoneoException('400', '3000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('400', '3002', 'EoneoPay\Exception\InalidPaymentSourceException');
        EoneoPay::registerEoneoException('400', '3009', 'EoneoPay\Exception\InvalidPaymentSourceException');
        EoneoPay::registerEoneoException('500', '3010', 'EoneoPay\Exception\TokenisationFailedException');
        EoneoPay::registerEoneoException('404', '3100', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('400', '3300', 'EoneoPay\Exception\DeleteDefaultPaymentSourceException');

        //Reference number allocation exceptions
        EoneoPay::registerEoneoException('400', '2200', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('409', '2201', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('400', '2210', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('409', '2211', 'EoneoPay\Exception\ResourceNotFoundException');

        //EWallet exceptions
        EoneoPay::registerEoneoException('404', '14100', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('404', '14101', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('409', '14102', 'EoneoPay\Exception\DeleteSourceWithFundsException');
        EoneoPay::registerEoneoException('409', '14103', 'EoneoPay\Exception\DeleteDefaultPaymentSourceException');
    }

    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        return "customers";
    }

    static protected function getRequiredProperties()
    {
        return ['email'];
    }

    /**
     * Add a credit card to this customer.
     *
     * This method calls the API to store the credit card and retrieve
     * a token.
     */
    public function addCreditCard(CreditCard $creditCard)
    {
        $creditCard->customer_id = $this->id;
        $savedCreditCard = $creditCard->save();
        return $savedCreditCard;
    }

    /**
     * Add a bank account to this customer.
     *
     * This method calls the API to store the bank account and retrieve
     * a token.
     */
    public function addBankAccount(BankAccount $bankAccount)
    {
        $bankAccount->customer_id = $this->id;
        return $bankAccount->save();
    }

    /**
     * Add a new subscription.
     */
    public function addSubscription(Subscription $subscription)
    {
        $subscription->customer_id = $this->id;
        return $subscription->save();
    }

    /**
     * Allocate a new BPAY CRN.
     */
    public function allocateBpayCrn()
    {
        $response = static::makeRequest(EoneoPay::PUT, static::getEndPoint() . "/" . $this->id . "/bpayCrn");
        if ($response->getStatusCode() == 200) {
            $object = json_decode($response->getBody());
            $object->crn = $object->bpayCrn;
            return $object;
        }

        return null;
    }

    /**
     * Allocate a new AusPost CRN.
     */
    public function allocateAusPostCrn()
    {
        $response = static::makeRequest(EoneoPay::PUT, static::getEndPoint() . "/" . $this->id . "/ausPostCrn");
        if ($response->getStatusCode() == 200) {
            $object = json_decode($response->getBody());
            $object->crn = $object->ausPostCrn;
            return $object;
        }

        return null;
    }

    /**
     * Allocate a new Allocated EFT ID
     */
    public function allocateEftId()
    {
        $response = static::makeRequest(EoneoPay::PUT, static::getEndPoint() . "/" . $this->id . "/eftId");
        if ($response->getStatusCode() == 200) {
            $object = json_decode($response->getBody());
            $object->crn = $object->eftId;
            return $object;
        }

        return null;
    }

    public function createEwallet($clearingAccount = 3)
    {
        $response = static::makeRequest(EoneoPay::POST, static::getEndPoint() . "/" . $this->id . "/ewallet?clearingAccount=" . $clearingAccount);
        if ($response->getStatusCode() == 200) {
            $object = json_decode($response->getBody())->ewallet;
            return $object;
        }

        return null;
    }

    /**
     *
     */
    public function getEWallet()
    {
        $response = static::makeRequest(EoneoPay::GET, static::getEndPoint() . "/" . $this->id . "/ewallet");
        if ($response->getStatusCode() == 200) {
            $ewallet = Ewallet::getObjectFromResponse($response);
            return $ewallet;
        }

        return null;
    }

    /**
     *
     */
    public function getEWallets()
    {
        $response = static::makerequest(EoneoPay::GET, static::getEndPoint() . "/" . $this->id . "/ewallets");
        if ($response->getStatusCode() == 200) {
            return Ewallet::getListFromResponse($response);
        }

        return null;
    }

    /**
     * Get an AusPost bar code.
     */
    public function getAusPostBarcode($bpayCrn, $pipId, $amount)
    {
        $response = static::makeRequest(EoneoPay::GET, static::getEndPoint() . "/" . $this->id . "/ausPostBarcodeForCrn/" . $bpayCrn . "/withPIPId/" . $pipId . "/forAmount/" . $amount);
        if ($response->getStatusCode() == 200) {
            return $response->getBody();
        }

        return null;
    }

    /**
     * Get a BPAY Bill Advice.
     */
    public function getBPAYBillAdvice($bpayCrn, $billerCode, $horizontal = false)
    {
        $response = static::makeRequest(EoneoPay::GET, static::getEndPoint() . "/" . $this->id . "/bpayBillAdviceForCrn/" . $bpayCrn . "/withBillerCode/" . $billerCode . "?horizontal=" . ($horizontal ? 'true' : 'false'));
        if ($response->getStatusCode() == 200) {
            return $response->getBody();
        }

        return null;
    }

    static protected function getObjectForResponseValue($responseValue)
    {
        if (property_exists($responseValue, 'reference_number')) {
            $object = new CustomerReferenceNumber;
        } else if (property_exists($responseValue, 'pan')) {
            $object = new CreditCard;
        } else if (property_exists($responseValue, 'bsb')) {
            $object = new BankAccount;
        }
        if ($object) {
            foreach (get_object_vars($responseValue) as $name => $value) {
                $object->$name = $value;
            }
        }

        return $object;
    }
}

Customer::__init__();
