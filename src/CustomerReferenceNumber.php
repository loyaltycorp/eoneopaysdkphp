<?php

namespace EoneoPay;

class CustomerReferenceNumber
{
    public $updated_at;
    public $biller_code;
    public $created_at;
    public $customer_id;
    public $reference_number;
    public $version;
    public $type;
    public $allocated;
    public $allocation_date;
    public $deallocation_date;
}
