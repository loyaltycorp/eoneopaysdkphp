<?php 
namespace EoneoPay;

class ProfileCreditCard extends ProfileSource
{
    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        if (is_null($instance)) {
            return "merchants/0/cards";
        } else {
            return "merchants/$instance->merchantId/cards";
        }
    }

    static protected function getRequiredProperties()
    {
        return ['name', 'number', 'expiry_month', 'expiry_year', 'cvc'];
    }

    /**
     * When a credit card is added to a customer the API responds with a full customer
     * object listing all credit cards. The ful object must be parsed to obtain the credit
     * card details.
     */
    static protected function getObjectFromResponse($response, $instance = null, $root = false)
    {
        if ($root) {
            $object = json_decode($response->getBody());
        } else {
            $object = json_decode($response->getBody())->credit_card;
        }

        $result = new static();
        foreach (get_object_vars($object) as $name => $value) {
            $result->$name = $value;
        }

        return $result;
    }
}
