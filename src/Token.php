<?php

namespace EoneoPay;

/**
 * Class to create and manage customers.
 */
class Token extends Resource
{
    static function __init__()
    {
        //Customer exceptions
        EoneoPay::registerEoneoException('400', '9000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('500', '9010', 'EoneoPay\Exception\TokenisationFailedException');
        EoneoPay::registerEoneoException('404', '9100', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('404', '9101', 'EoneoPay\Exception\ResourceNotFoundException');
    }
    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        return "tokens";
    }

    static protected function getRequiredProperties()
    {
        return [];
    }
}
