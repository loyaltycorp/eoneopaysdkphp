<?php

namespace EoneoPay\Exception;

class RefundProcessingErrorException extends PaymentProcessingErrorException
{
}
