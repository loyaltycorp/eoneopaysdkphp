<?php

namespace EoneoPay\Exception;

use GuzzleHttp\Exception\RequestException;

class TokenisationFailedException extends EoneoException
{
    public function __construct(RequestException $exception)
    {
        parent::__construct($exception);
    }

    public function getMessage()
    {
        return $this->getEoneoErrorMessage();
    }
}
