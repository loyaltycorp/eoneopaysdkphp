<?php

namespace EoneoPay\Exception;

use GuzzleHttp\Exception\RequestException;

class PaymentProcessingFailedException extends EoneoException
{
    protected $details;

    public function __construct(RequestException $exception) 
    {
        parent::__construct($exception);

        if ($exception->hasResponse()) {
            $response = $exception->getResponse();
            $body = $response->getBody();
            $data = json_decode($body);
            if ($data && isset($data->details)) {
                $this->details = $data->details;
            }
        }
    }

    public function getDetails()
    {
        return $this->details;
    }
}
