<?php

namespace EoneoPay\Exception;

class AllocationMissingReferenceException extends EoneoException
{
}
