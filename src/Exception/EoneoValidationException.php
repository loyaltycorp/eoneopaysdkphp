<?php

namespace EoneoPay\Exception;

use GuzzleHttp\Exception\RequestException;

class EoneoValidationException extends EoneoException
{
    protected $missingAttributes;

    static function __init__()
    {
        EoneoPay::registerEoneoException('400', '1000', EoneoValidationException::class);
    }

    public function __construct(RequestException $exception)
    {
        parent::__construct($exception);
        $missingAttributes = [];
        if ($exception->hasResponse()) {
            $response = $exception->getResponse();
            $body = $response->getBody();
            $data = json_decode($body);
            if ($data && isset($data->missing)) {
                $this->missingAttributes = explode(",", $data->missing);
            }
        }
    }

    public function getMissingAttributes()
    {
        return $this->missingAttributes;
    }
}
