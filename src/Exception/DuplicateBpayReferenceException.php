<?php

namespace EoneoPay\Exception;

class DuplicateBPayReferenceException extends EoneoException
{
}
