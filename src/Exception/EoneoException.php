<?php

namespace EoneoPay\Exception;

use GuzzleHttp\Exception\RequestException;

class EoneoException extends \Exception
{
    protected $eoneoErrorCode;
    protected $eoneoErrorMessage;

    public function __construct(RequestException $exception) {
        parent::__construct($exception->getMessage(), $exception->getCode(), $exception);
        if ($exception->hasResponse()) {
            $response = $exception->getResponse();
            $body = $response->getBody();
            $data = json_decode($body);
            if ($data && isset($data->code)) {
                $this->eoneoErrorCode = $data->code;
            }
            if ($data && isset($data->message)) {
                $this->eoneoErrorMessage = $data->message;
            }
        }
    }

    public function getEoneoErrorCode()
    {
        return $this->eoneoErrorCode;
    }

    public function getEoneoErrorMessage()
    {
        return $this->eoneoErrorMessage;
    }
}
