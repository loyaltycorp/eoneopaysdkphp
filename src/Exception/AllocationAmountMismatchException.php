<?php

namespace EoneoPay\Exception;

use GuzzleHttp\Exception\RequestException;

class AllocationAmountMismatchException extends EoneoException
{
    protected $payment;

    public function __construct(RequestException $exception) 
    {
        parent::__construct($exception);

        if ($exception->hasResponse()) {
            $response = $exception->getResponse();
            $body = $response->getBody();
            $data = json_decode($body);
            if ($data && isset($data->payment)) {
                $this->payment = $data->payment;
            }
        }
    }

    public function getPayment()
    {
        return $this->payment;
    }
}
