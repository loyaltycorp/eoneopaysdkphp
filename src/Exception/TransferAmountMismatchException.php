<?php

namespace EoneoPay\Exception;

use GuzzleHttp\Exception\RequestException;

class TransferAmountMismatchException extends EoneoException
{
}
