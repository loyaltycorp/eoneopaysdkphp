<?php

namespace EoneoPay\Exception;

use GuzzleHttp\Exception\RequestException;

class InvalidPaymentTypeException extends EoneoException
{
}
