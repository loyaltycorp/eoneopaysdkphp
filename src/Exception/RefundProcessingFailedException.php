<?php

namespace EoneoPay\Exception;

class RefundProcessingFailedException extends PaymentProcessingFailedException
{
}
