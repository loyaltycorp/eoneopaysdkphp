<?php

namespace EoneoPay\Exception;

use GuzzleHttp\Exception\RequestException;

class PaymentFailedException extends EoneoException
{
    protected $payment;
    protected $details;

    public function __construct(RequestException $exception) 
    {
        parent::__construct($exception);

        if ($exception->hasResponse()) {
            $response = $exception->getResponse();
            $body = $response->getBody();
            $data = json_decode($body);
            if ($data) {
                if (isset($data->payment)) {
                    $this->payment = $data->payment;
                }
                if (isset($data->details)) {
                    $this->details = $data->details;
                }
            }
        }
    }

    public function getPayment()
    {
        return $this->payment;
    }

    public function getDetails()
    {
        return $this->details;
    }
}
