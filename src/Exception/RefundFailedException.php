<?php

namespace EoneoPay\Exception;

class RefundFailedException extends PaymentFailedException
{
}
