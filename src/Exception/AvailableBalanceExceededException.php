<?php

namespace EoneoPay\Exception;

class AvailableBalanceExceededException extends EoneoException
{
}
