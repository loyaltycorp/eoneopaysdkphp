<?php

namespace EoneoPay;

use EoneoPay\ExceptionHandler\ValidationExceptionHandler;

/**
 * Class to create and manage customers.
 */
class Payment extends Resource
{
    const PAYMENT_STATUS_PROCESSED = 1;
    const PAYMENT_STATUS_FAILED = -1;

    const PAYMENT_STATUS_CREATED = 0;
    const PAYMENT_STATUS_PENDING = 10;

    const TRANSFER_STATUS_PENDING = 0;
    const TRANSFER_STATUS_AVAILABLE = 1;
    const TRANSFER_STATUS_TRANSFERRED = 2;

    const REFUND_RETURN_FLAG = 0;
    const REFUND_RETURN_OBJECT = 1;

    static function __init__()
    {
        EoneoPay::registerEoneoException('400', '1000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('400', '1001', 'EoneoPay\Exception\InvalidAmountException');
        EoneoPay::registerEoneoException('400', '1002', 'EoneoPay\Exception\InvalidPaymentSourceException');
        EoneoPay::registerEoneoException('400', '1003', 'EoneoPay\Exception\AllocationAmountMismatchException');
        EoneoPay::registerEoneoException('400', '1004', 'EoneoPay\Exception\AllocationMissingReferenceException');
        EoneoPay::registerEoneoException('400', '1005', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('500', '1006', 'EoneoPay\Exception\PaymentProcessingErrorException');
        EoneoPay::registerEoneoException('500', '1007', 'EoneoPay\Exception\PaymentProcessingFailedException');
        EoneoPay::registerEoneoException('500', '1008', 'EoneoPay\Exception\PaymentFailedException');
        EoneoPay::registerEoneoException('404', '1100', 'EoneoPay\Exception\ResourceNotFoundException');
        EoneoPay::registerEoneoException('400', '1200', 'EoneoPay\Exception\NoTransactionIdException');
        EoneoPay::registerEoneoException('500', '1206', 'EoneoPay\Exception\RefundProcessingErrorException');
        EoneoPay::registerEoneoException('500', '1207', 'EoneoPay\Exception\RefundProcessingFailedException');
        EoneoPay::registerEoneoException('500', '1208', 'EoneoPay\Exception\RefundFailedException');
    }

    static protected function getIdProperty()
    {
        return "txn_id";
    }

    static protected function getEndPoint($instance = null)
    {
        return "payments";
    }

    static protected function getRequiredProperties()
    {
        return ['token', 'amount'];
    }

    public function refund($refundReturnType = Payment::REFUND_RETURN_FLAG)
    {
        $result = parent::delete($refundReturnType === Payment::REFUND_RETURN_OBJECT);
        if ($refundReturnType === Payment::REFUND_RETURN_OBJECT) {
            $result = $result->refund;
        }

        return $result;
    }

    public function submit()
    {
        return parent::save();
    }
}

Payment::__init__();
