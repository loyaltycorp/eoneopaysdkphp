<?php

namespace EoneoPay;

/**
 * This class represents a Plan associated fee.
 */
class PlanFee
{
    /** 
     * Fee triggers.
     *  
     * Initial fees are paid when the first payment on the plan is processed.
     * Terminal fees are paid when the last payment is processed.
     * Periodical fees are processed each billing period according to the proration setting.
     * Transactional fees are processed for each payment.
     */ 
    const TRIGGER_INITIAL = 'initial';
    const TRIGGER_TERMINAL = 'terminal';
    const TRIGGER_PERIODICAL = 'periodical';
    const TRIGGER_TRANSACTIONAL = 'transactional';

    /** 
     * Periodical fees are processed according to the proration setting.
     *  
     * First proration means the entire fee amount is charged against the first payment in each billing cycle.
     * All proration means the fee is calculation pro-rata against each payment in the billing period.
     * Last proration means the entire fee amount is charged against the last payment in each billing cycle. The
     * last payment is defined to be the payment that settles the outstanding amount for the period.
     */ 
    const PRORATION_FIRST = 'first';
    const PRORATION_ALL = 'all';
    const PRORATION_LAST = 'last';

    public $description = "";
    public $amount = 0;
    public $trigger = PlanFee::TRIGGER_PERIODICAL;
    public $proration = PlanFee::PRORATION_ALL;
}
