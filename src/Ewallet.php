<?php

namespace EoneoPay;

class Ewallet extends Resource
{
    static function __init__()
    {
    }

    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        return "customers/$instance->customer_id/ewallets";
    }

    static protected function getRequiredProperties()
    {
        return [];
    }

    public function getStatement()
    {
        if (empty($this->id)) {
            return null;
        }
        $response = static::makeRequest(EoneoPay::GET, static::getEndPoint($this) . "/" . $this->id . "/statement");
        if ($response->getStatusCode() == 200) {
            return json_decode($response->getBody());
        }

        return null;
    }
}

Ewallet::__init__();
