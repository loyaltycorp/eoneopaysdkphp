<?php

namespace EoneoPay;

class ProfileBankAccount extends ProfileSource
{
    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        if (is_null($instance)) {
            return "merchants/0/bankAccounts";
        } else {
            return "merchants/$instance->merchantId/bankAccounts";
        }
    }

    static protected function getRequiredProperties()
    {
        return ['name', 'number', 'bsb'];
    }

    static protected function getObjectFromResponse($response, $instance = null, $root = false)
    {
        if ($root) {
            $object = json_decode($response->getBody());
        } else {
            $object = json_decode($response->getBody())->bank_account;
        }

        $result = new static();
        foreach (get_object_vars($object) as $name => $value) {
            $result->$name = $value;
        }

        return $result;
    }
}
