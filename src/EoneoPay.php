<?php

namespace EoneoPay;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use EoneoPay\Exception\EoneoException;

/**
 * This class is the core EoneoPay client.
 *
 * All requests are processed through EoneoPay::makeRequest().
 *
 * Basic configuration properties are also set here including the 
 * API Key, the API version and the base URI.
 *
 * This class uses Guzzle HTTP to send API requests.
 */
class EoneoPay
{
    /**
     * HTTP method verbs
     */
    const GET = 'GET';
    const PUT = 'PUT';
    const POST = 'POST';
    const DELETE = 'DELETE';

    /**
     * Version stamp
     */
    const SDK_VERSION = "1.2.0";

    /**
     * The Guzzle client instance.
     */
    static protected $client = null;

    /**
     * The API base URI.
     */
    static protected $baseUri = 'https://api.eoneopay.com';

    /**
     * The API key to use for requests.
     */
    static protected $apiKey;

    /**
     * The masquerade identity
     */
    static protected $masqueradeIdentity = "";
    
    /**
     * The API version.
     */
    static protected $version = 1;

    /**
     * The request timeout in seconds.
     */
    static protected $timeout = 30.0;

    /**
     * The Guzzle http_errors request option value.
     *
     * Set to true to have Guzzle throw exceptions for HTTP protocol errors.
     * Set to false to have Guzzle return a response with the HTTP status code.
     */
    static protected $httpErrors = true;

    /**
     * When true throw Eoneo SDK exceptions. When false rethrow Guzzle exceptions.
     * If $httpErrprs is false no exceptions are thrown, i.e. this parameter only
     * takes effect when httpErrors in true.
     */
    static protected $eoneoSdkExceptions = false;

    /**
     * Eoneo exception handler registry.
     *
     * When a Guzzle exception is encountered the SDK attempts to locate
     * a suitable Eoneo exception handler. Exception handlers are registered
     * by HTTP status code and Eoneo error code. If not suitable handler is
     * found the default exception handler is used which wraps the 
     * Guzzle exception in an Eoneo exception and throws that.
     */
    static protected $eoneoExceptionHandlers = [];

    /**
     * Set the base URI for API requests.
     *
     * @param string $baseUri The base URI for API requests.
     */
    static public function setBaseUri($baseUri)
    {
        static::$baseUri = $baseUri;
        static::$client = null;
    }

    /**
     * Set the API key to use for requests
     *
     * @param string $apiKey The API key
     */
    static public function setApiKey($apiKey)
    {
        static::$apiKey = $apiKey;
    }

    /**
     * Masquerade as the identified merchant.
     *
     * Merchant can be identified by merchant ID or business name.
     */
    static public function masqueradeAsMerchant($merchantNameOrId)
    {
        static::$masqueradeIdentity = $merchantNameOrId;
    }

    /**
     * Clear any previously set masquerade identity.
     */
    static public function stopMasquerading()
    {
        static::$masqueradeIdentity = "";
    }

    /**
     * Set the API version
     *
     * @param int $version The version number.
     */
    static public function setVersion($version)
    {
        static::$version = $version;
    }

    /**
     * Set the HTTP request timeout in seconds.
     *
     * @param float $timeout The timeout in seconds
     */
    static public function setTimeout($timeout)
    {
        static::$timeout = $timeout;
        static::$client = null;
    }

    /**
     * Turn Guzzle http errors on, i.e. throw exceptions for 4XX and 5XX responses, or off.
     */
    static public function setHttpErrors($httpErrors = true)
    {
        static::$httpErrors = $httpErrors;
    }

    /**
     * Turn Eoneo exceptions on. 
     */
    static public function setEoneoExceptions($eoneoSdkExceptions = true)
    {
        if ($eoneoSdkExceptions && !static::$httpErrors) {
            throw new \Exception("HTTP errors are disabled. HTTP errors must be enabled before turning on Eoneo exceptions.");
        }
        static::$eoneoSdkExceptions = $eoneoSdkExceptions;
    }

    /**
     * Send an HTTP request to the API end point.
     *
     * @param string $method The method to use for the request
     * @param string $endPoint The request end point.
     * @param array $data The data to include with the request
     * @return A HTTP response with headers, body, etc
     */
    static public function makeRequest($method, $endPoint, $data = null, $key = null)
    {
        if (is_null(static::$client)) {
            static::$client = new Client([
                'base_uri' => static::$baseUri,
                'timeout'  => static::$timeout,
            ]);
        }

        $parameters = [
            "auth" => [is_null($key) ? static::$apiKey : $key, static::$masqueradeIdentity],
            "http_errors" => static::$httpErrors
        ];
        if (is_null($data) === false) {
            $parameters['form_params'] = $data;
        }
        $url = "v" . static::$version . "/$endPoint";

        try {
            $response = static::$client->request($method, $url, $parameters);
        } catch (RequestException $requestException) {
            EoneoPay::handleException($requestException);
        }

        return $response;
    }

    /**
     * Register an exception handler for a specific HTTP status code and Eoneo error code combination.
     */
    static public function registerEoneoException($httpStatusCode, $eoneoCode = 0, $exceptionHandler)
    {
        if (!array_key_exists($httpStatusCode, static::$eoneoExceptionHandlers)) {
            static::$eoneoExceptionHandlers[$httpStatusCode] = [];
        }
        $handlers = static::$eoneoExceptionHandlers[$httpStatusCode];
        $handlers[$eoneoCode] = $exceptionHandler;
        static::$eoneoExceptionHandlers[$httpStatusCode] = $handlers;
    }

    /**
     * Get any registered exception handler for the specified HTTP status code and Eoneo error code combination.
     */
    static protected function getExceptionHandler($httpStatusCode, $eoneoCode = 0)
    {
        $handler = false;
        if (array_key_exists($httpStatusCode, static::$eoneoExceptionHandlers)) {
            $handlers = static::$eoneoExceptionHandlers[$httpStatusCode];
            if (array_key_exists($eoneoCode, $handlers)) {
                $handler = $handlers[$eoneoCode];
            }
        }

        return $handler;
    }

    static public function handleException(RequestException $requestException)
    {
        if (static::$eoneoSdkExceptions) {
            //If Eoneo exceptions are enabled find a handler that has been registered for 
            //the HTTP status code and Eoneo error code
            $httpCode = $requestException->getCode();
            if ($requestException->hasResponse()) {
                $responseData = json_decode($requestException->getResponse()->getBody());
            } else {
                throw new EoneoException($requestException);
            }
            $eoneoCode = isset($responseData->code) ? $responseData->code : 0;

            $exceptionHandler = static::getExceptionHandler($httpCode, $eoneoCode);
            if (!$exceptionHandler) {
                //If no registered exception handlers are found use the base exception handler
                $exceptionHandler = "\EoneoPay\Exception\EoneoException";
            }
            throw new $exceptionHandler($requestException);
        }
        throw $requestException;
    }
}

