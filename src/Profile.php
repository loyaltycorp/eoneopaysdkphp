<?php

namespace EoneoPay;

/**
 * Class to create and manage merchants.
 */
class Profile extends Resource
{
    static function __init__()
    {
        //Customer exceptions
        EoneoPay::registerEoneoException('400', '2000', 'EoneoPay\Exception\EoneoValidationException');
        EoneoPay::registerEoneoException('409', '2001', 'EoneoPay\Exception\DuplicateCustomerException');
        EoneoPay::registerEoneoException('500', '2010', 'EoneoPay\Exception\TokenisationFailedException');
        EoneoPay::registerEoneoException('404', '2100', 'EoneoPay\Exception\ResourceNotFoundException');
    }
        
    static protected function getClassName()
    {
        return "merchant";
    }

    static protected function getIdProperty()
    {       
        return "id"; 
    }   
        
    static protected function getEndPoint($instance = null)
    {
        return "merchants";
    }

    static protected function getRequiredProperties()
    {
        return ['email', 'title', 'first_name', 'last_name', 'business_name', 'business_phone', 'business_website', 'abn'];
    }

    public function save()
    {
        $balance = false;
        $available_balance = false;
        if (property_exists($this, 'balance')) {
            $balance = $this->balance;
            unset($this->balance);
        }
        if (property_exists($this, 'available_balance')) {
            $available_balance = $this->available_balance;
            unset($this->available_balance);
        }
        $profile = parent::save();
        if ($balance) {
            $this->balance = $balance;
        }

        if ($available_balance) {
            $this->available_balance = $available_balance;
        }

        return $profile;
    }

    /**
     * Add a credit card to this customer.
     *
     * This method calls the API to store the credit card and retrieve
     * a token.
     */
    public function addCreditCard(ProfileCreditCard $creditCard)
    {
        $creditCard->merchantId = $this->id;
        return $creditCard->save();
    }

    /**
     * Add a bank account to this customer.
     *
     * This method calls the API to store the bank account and retrieve
     * a token.
     */
    public function addBankAccount(ProfileBankAccount $bankAccount)
    {
        $bankAccount->merchantId = $this->id;
        return $bankAccount->save();
    }

	/**
     * Update the merchant available balance
     */
    public function updateAvailableBalance()
    {
        $response = static::makeRequest(EoneoPay::PUT, static::getEndPoint() . "/" . $this->id . "/availableBalance");
        if ($response->getStatusCode() == 200) {
            $object = json_decode($response->getBody());
            return $object;
        }

        return null;
    }

    static protected function getObjectForResponseValue($responseValue)
    {
        if (property_exists($responseValue, 'pan')) {
            $object = new CreditCard;
        } else if (property_exists($responseValue, 'bsb')) {
            $object = new BankAccount;
        } else {
            return $responseValue;
        }

        foreach (get_object_vars($responseValue) as $name => $value) {
            $object->$name = $value;
        }

        return $object;
    }
}

Profile::__init__();
