<?php

namespace EoneoPay;

class CreditCard extends CustomerSource
{
    static function __init__()
    {
    }

    static protected function getIdProperty()
    {
        return "id";
    }

    static protected function getEndPoint($instance = null)
    {
        if (is_null($instance)) {
            return "customers/0/cards";
        } else {
            return "customers/$instance->customer_id/cards";
        }
    }

    static protected function getRequiredProperties()
    {
        return ['name', 'number', 'expiry_month', 'expiry_year', 'cvc'];
    }

    /**
     * When a credit card is added to a customer the API responds with a full customer
     * object listing all credit cards. The ful object must be parsed to obtain the credit
     * card details.
     */
    static protected function getObjectFromResponse($response, $instance = null, $root = false)
    {
        if ($root) {
            $object = json_decode($response->getBody());
        } else {
            if (!is_null($instance)) {

                $json = json_decode($response->getBody());
                if ( property_exists($json,'credit_card')) {
                    $credit_card = $json->credit_card;
                } elseif ( property_exists($json, 'new_credit_card')) {
                    $credit_card = $json->new_credit_card;
                }

                $object = new CreditCard;
                foreach (get_object_vars($credit_card) as $name => $value) {
                    $object->$name = $value;
                }
            } else {
                $object = json_decode($response->getBody())->credit_card;
            }
        }

        $result = new static();
        foreach (get_object_vars($object) as $name => $value) {
            $result->$name = $value;
        }

        return $result;
    }

    public function setDefault()
    {
        $response = static::makeRequest(EoneoPay::PUT, static::getEndPoint($this) . "/$this->id");
        if ($response->getStatusCode() == 200) {
            $object = static::getObjectFromResponse($response);
            $this->{static::getIdProperty()} = $object->{static::getIdProperty()};
            return $object;
        }

        return false;
    }

    public function isDefault()
    {
        return $this->default;
    }
}

CreditCard::__init__();
