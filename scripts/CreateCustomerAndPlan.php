<?php

require __DIR__ . "/../vendor/autoload.php";

use EoneoPay\EoneoPay;
use EoneoPay\Customer;
use EoneoPay\Plan;
use EoneoPay\Subscription;
use EoneoPay\PlanFee;
use EoneoPay\Balance;

echo "Environment (1. Local, 2. Staging, 3 Live): ";
$environment = trim(fgets(STDIN));
if ($environment== "1") {
    EoneoPay::setBaseUri("http://localhost:2200");
    echo "LOCAL\n\n";
} else if ($environment== "2") {
    EoneoPay::setBaseUri("http://api-dev.eoneopay.com");
    echo "STAGING\n\n";
} else {
    echo "LIVE\n\n";
}

echo "Enter merchant API Key: ";
$apiKey = trim(fgets(STDIN));

EoneoPay::setApiKey($apiKey);

//Validate the API Key
echo "Authenticating...";
Balance::retrieve();
echo "\nSuccess.\n\n";

echo "Customer email: ";
$emailAddress = trim(fgets(STDIN));
echo "Customer first name: ";
$firstName = trim(fgets(STDIN));
echo "Customer last name: ";
$lastName = trim(fgets(STDIN));

$customer = new Customer();
$customer->email = $emailAddress;
$customer->first_name = $firstName;
$customer->last_name = $lastName;

$customer->save();

$crn = $customer->allocateBpayCrn();

echo "Plan name: ";
$planName = trim(fgets(STDIN));
echo "Plan interval (1. Monthly, 2. Weekly): ";
$planInterval = trim(fgets(STDIN));
echo "Plan amount (cents): ";
$planAmount = trim(fgets(STDIN));

$plan = new Plan();
$plan->name = $planName;
$plan->interval = ["month", "week"][intval($planInterval) - 1];
$plan->interval_count = 1;
$plan->currency = 'AUD';
$plan->amount = $planAmount;

$plan->save();

//Add plan fees
do {
    $moreFees = false;
    echo "Add a plan fee (y/N)?";
    if (trim(fgets(STDIN)) == "y") {
        echo "Fee name: ";
        $feeName = trim(fgets(STDIN));
        echo "Fee amount (cents): ";
        $feeAmount = trim(fgets(STDIN));
        echo "Fee trigger (1. Periodical, 2. Transactional, 3. Initial, 4. Terminal): ";
        $feeTrigger = trim(fgets(STDIN));
        if (intval($feeTrigger) == 1) {
            echo "Fee proration (1. All, 2. First, 3. Last): ";
            $feeProration = trim(fgets(STDIN));
        } else {
            $feeProration = 'all';
        }

        $fee = new PlanFee;
        $fee->description = $feeName;
        $fee->amount = intval($feeAmount);
        $fee->trigger = ['periodical', 'transactional', 'initial', 'terminal'][intval($feeTrigger) - 1];
        $fee->proration = ['all', 'first', 'last'][intval($feeProration) - 1];

        $plan->addFee($fee);

        $moreFees = true;
    }
} while ($moreFees);

$plan->save();

echo "Subscription start date (yyyymmdd): ";
$startDate = trim(fgets(STDIN));

$bpay_reference_number = $crn->crn;

$subscription = new Subscription();
$subscription->plan = $plan->id;
$subscription->bpay_reference_number = $bpay_reference_number;
$subscription->start_date = $startDate;

$customer->addSubscription($subscription);

echo "BPAY CRN IS " . $bpay_reference_number . "\n\n";
