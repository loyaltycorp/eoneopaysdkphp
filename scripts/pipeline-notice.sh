#! /bin/bash

MESSAGE=Failed
COLOR=danger
if [ "$1" = "success" ]
then
    MESSAGE=Successful
    COLOR=good
fi

curl -X POST  -H 'Content-type: application/json' --data '{"attachments":[{"text":"'$MESSAGE'","pretext":"EoneoPayClientLibrary_PHP/'$BITBUCKET_BRANCH' - '$BITBUCKET_COMMIT'","color":"'$COLOR'"}]}' https://hooks.slack.com/services/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
