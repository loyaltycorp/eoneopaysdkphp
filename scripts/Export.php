<?php

require __DIR__ . "/../vendor/autoload.php";

use EoneoPay\EoneoPay;
use EoneoPay\Customer;
use EoneoPay\Plan;
use EoneoPay\Subscription;
use EoneoPay\PlanFee;
use EoneoPay\Balance;

function writeToCsv($objects, $filename)
{
    if (is_array($objects) && count($objects) > 0) {
        $headers = get_object_vars($objects[0]);
        $file = fopen($filename, "w");
        fputcsv($file, array_keys($headers));
        foreach ($objects as $object) {
            if ($object) {
                $csv = [];
                $values = get_object_vars($object);
                foreach ($headers as $name => $value) {
                    if (is_array($values[$name])) {
                        $csv[] = json_encode($value);
                    } else {
                        $csv[] = $values[$name];
                    }
                }
                fputcsv($file, $csv);
            }
        }
        fclose($file);
    }
}

echo "Environment (1. Local, 2. Staging, 3 Live): ";
$environment = trim(fgets(STDIN));
if ($environment== "1") {
    EoneoPay::setBaseUri("http://localhost:2200");
    echo "LOCAL\n\n";
} else if ($environment== "2") {
    EoneoPay::setBaseUri("http://api-staging.eoneopay.com");
    echo "STAGING\n\n";
} else {
    echo "LIVE\n\n";
}

echo "Enter merchant API Key: ";
$apiKey = trim(fgets(STDIN));

EoneoPay::setApiKey($apiKey);

//Validate the API Key
echo "Authenticating...";
Balance::retrieve();
echo "\nSuccess.\n\n";

echo "Dumping plans...";
writeToCsv(Plan::all(), 'plans.csv');
$customers = Customer::all();
echo "\nDumping customers...";
writeToCsv($customers, 'customer.csv');
echo "\nGathering subscriptions";
foreach ($customers as $customer) {
    $subscriptions[] = Subscription::getList($customer->id)[0];
    echo ".";
}
echo "\nDumping subscriptions...";
writeToCsv($subscriptions, 'subscriptions.csv');
echo "\nDone\n\n";
