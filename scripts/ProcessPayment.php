<?php

require __DIR__ . "/../vendor/autoload.php";

use EoneoPay\EoneoPay;
use EoneoPay\Customer;
use EoneoPay\Profile;
use EoneoPay\CreditCard;
use EoneoPay\Balance;
use EoneoPay\Payment;

echo "Environment (1. Local, 2. Staging, 3 Live): ";
$environment = trim(fgets(STDIN));
if ($environment== "1") {
    EoneoPay::setBaseUri("http://localhost:2200");
    echo "LOCAL\n\n";
} else if ($environment== "2") {
    EoneoPay::setBaseUri("http://api-dev.eoneopay.com");
    echo "STAGING\n\n";
} else {
    echo "LIVE\n\n";
}

echo "Enter merchant API Key: ";
$apiKey = trim(fgets(STDIN));

EoneoPay::setApiKey($apiKey);

//Validate the API Key
echo "Authenticating...";
Balance::retrieve();
echo "\nSuccess.\n\n";

echo "Process payment from merchant account (y/N)?";
if (trim(fgets(STDIN)) == "n") {
    $selectedCustomer = false;
    $useExistingPaymentSource = false;
    do {
        echo "Process payment for existing customer (y/N)?";
        if (trim(fgets(STDIN)) == "y") {
            echo "Enter last name of customer: ";
            $lastName = trim(fgets(STDIN));
            $customers = Customer::all(['last_name' => $lastName], 20);

            if (!empty($customers)) {
                $index = 1;
                foreach ($customers as $customer) {
                    echo ($index++) . ". $customer->first_name $customer->last_name ($customer->email)\n";
                }

                do {
                    echo "Select a customer: ";
                    $selectedIndex = intval(trim(fgets(STDIN)));
                    if ($selectedIndex >= 1 && $selectedIndex <= sizeof($customers)) {
                        $selectedCustomer = $customers[$selectedIndex - 1];
                    } else {
                        echo "Please enter a valid selection between 1 and " . sizeof($customers) . ".\n";
                    }
                } while (!$selectedCustomer);
            }
        } else {
            echo "Customer email: ";
            $emailAddress = trim(fgets(STDIN));
            echo "Customer first name: ";
            $firstName = trim(fgets(STDIN));
            echo "Customer last name: ";
            $lastName = trim(fgets(STDIN));

            $customer = new Customer();
            $customer->email = $emailAddress;
            $customer->first_name = $firstName;
            $customer->last_name = $lastName;

            $customer->save();

            $selectedCustomer = $customer;
        }
    } while (!$selectedCustomer);
} else {
    $selectedCustomer = Profile::retrieve();
}

if (!empty($selectedCustomer->credit_cards)) {
    echo "Use an existing payment source (y/N)?";
    $useExistingPaymentSource = trim(fgets(STDIN)) == "y";
}

$selectedPaymentSource = false;
if ($selectedCustomer && $useExistingPaymentSource && !empty($selectedCustomer->credit_cards)) {
    do {
        $creditCards = [];
        foreach ($selectedCustomer->credit_cards as $creditCardId) {
            if (is_object($creditCardId)) {
                $creditCards[] = $creditCardId;
            } else {
                $creditCards[] = CreditCard::retrieve($creditCardId);
            }
        }
        $index = 1;
        foreach ($creditCards as $creditCard) {
            echo ($index++) . ". $creditCard->pan\n";
        }
        echo "Select a card: ";
        $selectedIndex = intval(trim(fgets(STDIN)));
        if ($selectedIndex >= 1 && $selectedIndex <= sizeof($creditCards)) {
            $selectedPaymentSource = $creditCards[$selectedIndex - 1];
        }
    } while (!$selectedPaymentSource);
}

if (!$selectedPaymentSource) {
    echo "\n\nName on card: ";
    $name = trim(fgets(STDIN));
    echo "Number: ";
    $number = trim(fgets(STDIN));
    echo "Expiry month: ";
    $expiryMonth = trim(fgets(STDIN));
    echo "Expiry year: ";
    $expiryYear = trim(fgets(STDIN));
    echo "CVC: ";
    $cvc = trim(fgets(STDIN));

    $selectedPaymentSource = new CreditCard();
    $selectedPaymentSource->number = $number;
    $selectedPaymentSource->name = $name;
    $selectedPaymentSource->expiry_month = $expiryMonth;
    $selectedPaymentSource->expiry_year = $expiryYear;
    $selectedPaymentSource->cvc = $cvc;
    $selectedPaymentSource->customer_id = $selectedCustomer->id;
    $selectedPaymentSource->save();
}

if ($selectedCustomer && $selectedPaymentSource) {
    echo "\n\nPayment amount (in cents): ";
    $amount = trim(fgets(STDIN));
    echo "Your reference: ";
    $reference = trim(fgets(STDIN));
    echo "Clearing account (leave blank for default): ";
    $clearingAccount = trim(fgets(STDIN));

    $payment = new Payment();
    $payment->amount = intval($amount);
    $payment->token = $selectedPaymentSource->id;
    $payment->reference = $reference;
    if (!empty($clearingAccount)) {
        $payment->clearing_account = $clearingAccount;
    }
    $payment->submit();

    echo "\n\nTransaction ID: $payment->txn_id\n\n";
}
