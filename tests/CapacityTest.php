<?php

require __DIR__ . "/../vendor/autoload.php";

use EoneoPay\EoneoPay;
use EoneoPay\Profile;
use EoneoPay\Customer;
use EoneoPay\CustomerSource;
use EoneoPay\CreditCard;
use EoneoPay\BankAccount;
use EoneoPay\Payment;
use EoneoPay\Balance;
use EoneoPay\Plan;
use EoneoPay\Transfer;
use EoneoPay\Subscription;
use EoneoPay\PlanFee;
use EoneoPay\Token;
use EoneoPay\Charge;
use EoneoPay\TestBatchPayment;

class CapacityTest extends TestCase
{
    /**
     * Set up the API end point and key for each test.
     */
    public function setup()
    {
        //Set the API end point
        EoneoPay::setBaseUri($_ENV['EONEOPAY_ENDPOINT']);

        //Set the merchant API key
        EoneoPay::setApiKey($_ENV['EONEOPAY_API_KEY']);

        //Set API timeout
        EoneoPay::setTimeout(120.0);
    }

    public function testCustomerCreateAndRetrieve()
    {
        /*$customerId = [];
        for ($i = 0; $i < 1000; $i++) {
            //Create a new customer
            $customer = new Customer;
            $customer->first_name = 'First'.$i;
            $customer->last_name = 'Last'.$i;
            $customer->email = "test$i@example.com";
            $customer = $customer->save();

            $customerId[] = $customer->id;
        }

        for ($i = 0; $i < 1000; $i++) {
        //Retrieve the customer through the API
            $retrievedCustomer = Customer::retrieve($customerId[$i]);
        }

        for ($i = 0; $i < 1000; $i++) {
            $retrievedCustomer = Customer::retrieve($customerId[$i]);
            $retrievedCustomer->delete();
        }*/
    }

    public function testPlansAndSubscriptions()
    {
        /*$plan = new Plan;
        $plan->name = 'Test Plan';
        $plan->amount = 100;
        $plan->interval = Plan::INTERVAL_MONTH;
        $plan->intervalCount = 1;

        $retrievedPlan = $plan->save();

        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_subscriptions@example.com';
        $retrievedCustomer = $customer->save();

        $subscription = new Subscription;
        $subscription->plan = $plan->id;
        $subscription->start_date = date("Y-m-d");
        $subscription->end_date = strtotime("+1 year", time());
        $newSubscription = $retrievedCustomer->addSubscription($subscription);

        for ($i = 0; $i < 100; $i++) {
            $retrievedSubscription = Subscription::retrieve($newSubscription->id);
        }

        $creditCard = new CreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';

        //Add the credit card to the customer, calls the API to store the card and obtain a token
        $customer->addCreditCard($creditCard);

        $payment = new Payment;
        $payment->amount = $i * 100;
        $payment->token = $creditCard->id;
        $payment->reference = "Test payment $i.";
        $payment->reference_number = $newSubscription->id;
        $processedPayment = $payment->submit();

        for ($i = 0; $i < 100; $i++) {
            $paymentList = $subscription->getStatement();
            $paymentList = $subscription->getPayments();
        }*/
    }
}
