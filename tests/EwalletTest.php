<?php

require __DIR__ . "/../vendor/autoload.php";

use EoneoPay\EoneoPay;
use EoneoPay\Profile;
use EoneoPay\Customer;
use EoneoPay\CustomerSource;
use EoneoPay\CreditCard;
use EoneoPay\BankAccount;
use EoneoPay\Payment;
use EoneoPay\Balance;
use EoneoPay\Plan;
use EoneoPay\Transfer;
use EoneoPay\Subscription;
use EoneoPay\PlanFee;
use EoneoPay\Token;
use EoneoPay\Charge;
use EoneoPay\TestBatchPayment;
use EoneoPay\Ewallet;

class EwalletTest extends TestCase
{
    public function testCanUserCreateMultipleEwalletsForCustomer()
    {
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $retrievedCustomer = $customer->save();


        $newEwallet = new Ewallet;
        $newEwallet->customer_id = $retrievedCustomer->id;
        $newEwallet->save();

        $ewallets = $customer->getEWallets();

        $this->assertCount(2, $ewallets);
    }

    public function testCanUserDeleteAnEwallet()
    {
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $retrievedCustomer = $customer->save();

        $newEwallet = new Ewallet;
        $newEwallet->customer_id = $retrievedCustomer->id;
        $retrievedEwallet = $newEwallet->save();

        $ewallets = $customer->getEWallets();
        $this->assertCount(2, $ewallets);

        $retrievedEwallet->delete();

        $ewallets = $customer->getEWallets();
        $this->assertCount(1, $ewallets);
    }

    public function testCanUserNotPayWithLockedEwallet()
    {
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $retrievedCustomer = $customer->save();

        $retrievedEwallet = $retrievedCustomer->getEWallet();
        $futureDate = new \DateTime;
        $futureDate->add(new \DateInterval('P1D'));
        //Lock down the ewallet until tomorrow
        $retrievedEwallet->locked_until_date = $futureDate->format('Y-m-d H:i:s');
        $retrievedEwallet->save();

        $plan = new Plan;
        $plan->name = 'Test Plan';
        $plan->amount = 10000;
        $plan->interval = Plan::INTERVAL_MONTH;
        $plan->interval_count = 1;

        $retrievedPlan = $plan->save();

        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_subscriptions@example.com';
        $retrievedCustomer = $customer->save();

        $creditCard = new CreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';

        //Add the credit card to the customer, calls the API to store the card and obtain a token
        $retrievedCustomer->addCreditCard($creditCard);

        //Put cash on the ewallet from our credit card
        $payment = new Payment;
        $payment->amount = 200000;
        $payment->token = $creditCard->id;
        $payment->reference = 'Test payment.';
        $payment->reference_number = $retrievedEwallet->id;
        $processedPayment = $payment->submit();

        $subscription = new Subscription;
        $subscription->plan = $plan->id;
        $subscription->start_date = date("Y-m-d");
        $subscription->end_date = strtotime("+1 year", time());
        $newSubscription = $retrievedCustomer->addSubscription($subscription);

        $retrievedSubscription = Subscription::retrieve($newSubscription->id);

        $paymentFailed = false;
        try {
            //Try to pay with our locked ewallet, which should not work
            $payment = new Payment;
            $payment->amount = 11165;
            $payment->token = $retrievedEwallet->source_id;
            $payment->reference = 'Test payment.';
            $payment->reference_number = $subscription->id;
            $processedPayment = $payment->submit();
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\PaymentProcessingFailedException", get_class($e));
            $this->assertEquals(500, $e->getCode());
            $paymentFailed = true;
        } finally {
            if (!$paymentFailed) {
                $this->fail("Payment should fail because ewallet is locked down until tomorrow");
            }
        }
    }

    public function testCanUserNotDeletePrimaryEwallet()
    {
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $retrievedCustomer = $customer->save();
        $retrievedEwallet = $retrievedCustomer->getEwallet();

        $deleteFailed = false;
        try {
            $retrievedEwallet->delete();
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\DeleteDefaultPaymentSourceException", get_class($e));
            $this->assertEquals(409, $e->getCode());
            $deleteFailed = true;
        } finally {
            if (!$deleteFailed) {
                $this->fail("Deletion should fail because we're trying to delete a primary ewallet.");
            }
        }

        $ewallets = $customer->getEWallets();
        $this->assertCount(1, $ewallets);
    }

    public function testCanUserGetEwalletStatement()
    {
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $retrievedCustomer = $customer->save();
        $retrievedEwallet = $retrievedCustomer->getEwallet();
        $statement = $retrievedEwallet->getStatement();
        $this->assertTrue(is_object($statement));
    }
}
