<?php

require __DIR__ . "/../vendor/autoload.php";

use EoneoPay\EoneoPay;
use EoneoPay\Profile;
use EoneoPay\Customer;
use EoneoPay\CustomerSource;
use EoneoPay\CreditCard;
use EoneoPay\BankAccount;
use EoneoPay\Payment;
use EoneoPay\Balance;
use EoneoPay\Plan;
use EoneoPay\Transfer;
use EoneoPay\Subscription;
use EoneoPay\PlanFee;
use EoneoPay\Token;
use EoneoPay\Charge;
use EoneoPay\TestBatchPayment;

class EoneoPayClientTest extends TestCase
{

    public function testRetrieveAndModifyOwnProfile()
    {
        $profile = Profile::retrieve();

        $profile->email = 'updated@sdk.client';
        $profile->save();

        $profile = Profile::retrieve('self');
        $this->assertEquals('updated@sdk.client', $profile->email);

        $profile->updateAvailableBalance();
    }

    /**
     * Test a simple customer create and retrieve operation.
     *
     * This test also deletes the customer but does not confirm the 
     * customer is deleted through an API call. The testCreateAndDeleteCustomer 
     * test does that.
     */
    public function testCustomerCreateAndRetrieve()
    {
        //Create a new customer
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last'.time();
        $customer->email = 'test1@example.com';
        $customer = $customer->save();

        //Retrieve the customer through the API
        $retrievedCustomer = Customer::retrieve($customer->id);

        $externalId = time();
        $retrievedCustomer->external_customer_id = $externalId;
        $retrievedCustomer->save();

        //Confirm the ID and email address match
        $this->assertEquals($retrievedCustomer->id, $customer->id);
        $this->assertEquals($retrievedCustomer->email, $customer->email);

        //Retrieve the customer through the API using external customer id
        $retrievedCustomer = Customer::retrieve($customer->external_customer_id);

        //Confirm the ID and email address match
        $this->assertEquals($retrievedCustomer->id, $customer->id);
        $this->assertEquals($retrievedCustomer->email, $customer->email);

        $done = false;
        $found = false;
        $offset = 0;
        while (!$done && !$found) {
            $customers = Customer::all(['last_name' => $customer->last_name], 10, $offset++);
            $done = sizeof($customers) == 0;
            if (!$done) {
                foreach ($customers as $listedCustomer) {
                    $found = $listedCustomer->id == $retrievedCustomer->id;
                    if ($found) {
                        break;
                    }
                }
            }
        }
        $this->assertTrue($found);

        //delete the customer
        $retrievedCustomer->delete();

        //Attempt to create a new customer with insufficient details
        $createFailed = false;
        try {
            $customer = new Customer;
            $customer = $customer->save();
        } catch (Exception $e) {
            $this->assertEquals("EoneoPay\Exception\EoneoValidationException", get_class($e));
            $this->assertEquals(400, $e->getCode());
            $missingAttributes = $e->getMissingAttributes();
            $this->assertTrue(is_array($missingAttributes));
            $this->assertEquals(2, sizeof($missingAttributes));
            $this->assertTrue(in_array('first_name', $missingAttributes));
            $this->assertTrue(in_array('last_name', $missingAttributes));
            $createFailed = true;
        } finally {
            if (!$createFailed) {
                $this->fail('Customer successfully created with insufficient details provided.');
            }
        }

    }

    /**
     * Create a new customer, update the email address and confirm the update is
     * accurate and effective.
     */
    public function testCreateAndUpdateCustomer()
    {
        //Create a new customer
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test2@example.com';
        $customer = $customer->save();

        //Retrieve the customer and confirm email address matches
        $retrievedCustomer = Customer::retrieve($customer->id);
        $this->assertEquals($retrievedCustomer->email, 'test2@example.com');

        //Update the email address
        $customer->email = 'test3@example.com';
        $customer = $customer->save();

        //Retrieve the customer and confirm email address matches
        $updatedCustomer = Customer::retrieve($customer->id);
        $this->assertEquals($updatedCustomer->email, 'test3@example.com');
    }

    /**
     * Create a customer and delete the customer, confirming the deletion
     * is effective by attempting to fetch the customer again through the API.
     */
    public function testCreateAndDeleteCustomer()
    {
        //Create a new customer
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test4@example.com';
        $customer = $customer->save();

        //Retrieve the customer through the API
        $retrievedCustomer = Customer::retrieve($customer->id);
        $this->assertEquals($retrievedCustomer->email, 'test4@example.com');

        //Delete the customer
        $retrievedCustomer->delete();

        $retrieveFailed = false;
        try {
            $deletedCustomer = Customer::retrieve($retrievedCustomer->id);
        } catch (Exception $e) {
            //This is the expected response, confirm the exception is a GuzzleHttp ClientException 
            $this->assertEquals("EoneoPay\Exception\ResourceNotFoundException", get_class($e));

            //and confirm the response status code is 404
            $this->assertEquals($e->getCode(), 404);
            $retrieveFailed = true;
        } finally {
            if (!$retrieveFailed) {
                //Fail if the customer is successfull retrieved, should get a 404 response/exception
                $this->fail('Deleted customer successfully retrieved!');
            }
        }
    }

    /**
     * Test credit card and bank account operations by adding one of each
     * to a new customer.
     */
    public function testCustomerSources()
    {
        //Create a new customer
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $customer->save();

        $customerId = $customer->id;

        //Create a new credit card
        $creditCard = new CreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';

        //Add the credit card to the customer, calls the API to store the card and obtain a token
        $customer->addCreditCard($creditCard);

        $retrievedCreditCard = CreditCard::retrieve($creditCard->id);
        //$this->assertTrue($retrievedCreditCard->isDefault());

        //Test update card details
        $retrievedCreditCard->cvc = '456';
        $retrievedCreditCard->save();

        $retrievedCreditCard = CreditCard::retrieve($creditCard->id);
        $this->assertEquals('456', $retrievedCreditCard->cvc);

        $secondCreditCard = new CreditCard;
        $secondCreditCard->number = '5431111111111111';
        $secondCreditCard->expiry_month = 11;
        $secondCreditCard->expiry_year = 16;
        $secondCreditCard->name = 'Mr Example2';
        $secondCreditCard->cvc = '456';

        //Add the credit card to the customer, calls the API to store the card and obtain a token
        $customer->addCreditCard($secondCreditCard);

        $retrievedPaymentSource = CustomerSource::retrieve($secondCreditCard->id);
        $this->assertTrue($retrievedPaymentSource !== false);
        $this->assertTrue(isset($retrievedPaymentSource->type));
        $this->assertEquals("credit_card", $retrievedPaymentSource->type);
        $this->assertEquals($customer->id, $retrievedPaymentSource->customer_id);
        $this->assertEquals(substr($secondCreditCard->number, 0, 6)."XXXXXXX".substr($secondCreditCard->number, -3), $retrievedPaymentSource->pan);

        $retrievedCreditCard = CreditCard::retrieve($secondCreditCard->id);

        $this->assertFalse($retrievedCreditCard->isDefault());

        $retrievedCreditCard->setDefault();

        $retrievedCreditCard = CreditCard::retrieve($creditCard->id);
        $this->assertFalse($retrievedCreditCard->isDefault());

        $retrievedCreditCard = CreditCard::retrieve($secondCreditCard->id);
        $this->assertTrue($retrievedCreditCard->isDefault());

        //Create a new bank account
        $bankAccount = new BankAccount();
        $bankAccount->number = '012345';
        $bankAccount->bsb = '123-456';
        $bankAccount->name = 'Mr Example Account';

        //Add the bank account to the customer, calls the API to store the bank account and obtain a token
        $customer->addBankAccount($bankAccount);

        $retrievedBankAccount = BankAccount::retrieve($bankAccount->id);

        //Test update bank account details
        $retrievedBankAccount->bsb = '987-654';
        $retrievedBankAccount->save();
        
        $retrievedPaymentSource = CustomerSource::retrieve($bankAccount->id);
        $this->assertTrue($retrievedPaymentSource !== false);
        $this->assertTrue(isset($retrievedPaymentSource->type));
        $this->assertEquals("bank_account", $retrievedPaymentSource->type);
        $this->assertEquals($customer->id, $retrievedPaymentSource->customer_id);
        $this->assertEquals($retrievedBankAccount->bsb, $retrievedPaymentSource->bsb);
        $this->assertEquals($bankAccount->number, $retrievedPaymentSource->number);

        $retrievedBankAccount = BankAccount::retrieve($bankAccount->id);

        $this->assertTrue((bool)$retrievedBankAccount->isDefault());

        $retrievedBankAccount->setDefault();

        //Retrieve the customer through the API
        $retrievedCustomer = Customer::retrieve($customerId);

        //Assert the credit card PAN is correct, also confirms the credit card is stored and retrieved correctly
        $this->assertTrue($retrievedCustomer->credit_cards[0]->pan == '444433XXXXXXX111');

        //Assert the bank account number is correct, also confirms the bank account is stored and retrieved correctly
        $this->assertTrue($retrievedCustomer->bank_accounts[0]->number == '012345');

        //Test deleting bank accounts and credit cards directly from the Customer following failure in G's testing
        ////Cannot delete default payment source from release 0.2.0
        $retrievedCustomer->bank_accounts[0]->delete();
        $retrievedCustomer->credit_cards[0]->delete();
        $retrievedCustomer->delete();
    }

    /**
     * Test tokens
     */
    public function testTokens()
    {
        //Create a new token
        $creditCard = new CreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';

        $token = new Token;
        $token->card = $creditCard;
        $savedToken = $token->save();
        $this->assertEquals($savedToken->source->info->cardDescription, 'Visa');
        $this->assertEquals($savedToken->source->info->cardType, 6);
        $this->assertEquals($savedToken->source->info->pan, '444433XXXXXXX111');

        $retrievedToken = Token::retrieve($savedToken->id);
        $this->assertEquals($retrievedToken->source->info->cardDescription, 'Visa');
        $this->assertEquals($retrievedToken->source->info->cardType, 6);
        $this->assertEquals($retrievedToken->source->info->pan, '444433XXXXXXX111');

        $payment = new Payment;
        $payment->amount = 100;
        $payment->token = $savedToken->id;
        $payment->reference = 'Test payment.';
        $processedPayment = $payment->submit();

        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $customer->save();

        $tokenCard = new CreditCard;
        $tokenCard->token = $savedToken->id;
        $retrievedCreditCard = $customer->addCreditCard($tokenCard);
        var_dump($retrievedCreditCard);
        $this->assertEquals($creditCard->name, $retrievedCreditCard->name);
        $this->assertEquals('444433XXXXXXX111', $retrievedCreditCard->pan);
        $this->assertEquals($customer->id, $retrievedCreditCard->customer_id);

        $updatedCustomer = Customer::retrieve($customer->id);
        $this->assertEquals($updatedCustomer->id, $updatedCustomer->credit_cards[0]->customer_id);
    }

    /**
     * Create a credit card payment and refund it.
     */
    public function testPayments()
    {
        //Create a new customer
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $retrievedCustomer = $customer->save();

        //Add a credit card
        $creditCard = new CreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';

        $retrievedCreditCard = $customer->addCreditCard($creditCard);

        //Create a new payment of $1, note the amount is in cents
        //Add the credit card token to the payment
        $payment = new Payment;
        $payment->amount = 100;
        $payment->token = $retrievedCreditCard->id;
        $payment->reference = 'Test payment.';
        $payment->statement_descriptor = 'Statement descriptor';
        $processedPayment = $payment->submit();

        //Confirm the payment has been processed and the correct token is associated.
        $this->assertEquals($processedPayment->token, $creditCard->id);
        $this->assertEquals($processedPayment->amount, $payment->amount);
        $this->assertEquals($processedPayment->customer_id, $retrievedCustomer->id);

        //Attempt to retrieve a payment with an invalid txn_id
        $paymentFailed = false;
        try {
            $retrievedPayment = Payment::retrieve('somerandominvalidid');
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\ResourceNotFoundException", get_class($e));
            $paymentFailed = true;
        } finally {
            if (!$paymentFailed) {
                $this->fail('Payment processing expected to fail for payment not found but processing was successful.');
            }
        }

        $retrievedPayment = Payment::retrieve($payment->txn_id);
        $this->assertEquals(Payment::PAYMENT_STATUS_PROCESSED, $retrievedPayment->status);
        //Refund the payment
        $refunded = $retrievedPayment->refund();
        $this->assertTrue($refunded);

        //Delete the credit card
        //Cannot delete default payment source from release 0.2.0
        //$retrievedCreditCard->delete();

        //Confirm payment listing is valid for this customer
        $aPayments = Payment::all(["customer_id", $retrievedCustomer->id]);
        $this->assertTrue(is_array($aPayments));
        $this->assertEquals(2, count($aPayments)); //2 including the refund
        $this->assertEquals($retrievedCustomer->id, $aPayments[0]->customer_id);
        $this->assertTrue($processedPayment->txn_id == $aPayments[0]->txn_id || $processedPayment->txn_id == $aPayments[1]->txn_id);

        //Test a failed payment
        $paymentFailed = false;
        try {
            $payment = new Payment;
            $payment->amount = 101;
            $payment->token = $retrievedCreditCard->id;
            $payment->reference = 'Test payment.';
            $payment->statement_descriptor = 'Statement descriptor';
            $payment->test_validate_amount = true;
            $processedPayment = $payment->submit();
        } catch (Exception $e) {
            //Payment failed as expected, continue testing
            $paymentFailed = true;
        } finally {
            if (!$paymentFailed) {
                //Payment should have failed and thrown an exception
                $this->fail('Payment processing expected to fail for amount validation but processing was successful.');
            }
        }

        //Attempt to send a payment with no amount attribute
        try {
            $payment = new Payment;
            $payment->token = $retrievedCreditCard->id;
            $payment->reference = 'Test payment.';
            $payment->statement_descriptor = 'Statement descriptor';
            $processedPayment = $payment->submit();
            $this->fail("Payment should fail for missing amount attribute but it succeeded");
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\EoneoValidationException", get_class($e));
            $this->assertEquals(400, $e->getCode());
            $missingAttributes = $e->getMissingAttributes();
            $this->assertTrue(is_array($missingAttributes));
            $this->assertEquals(1, sizeof($missingAttributes));
            $this->assertEquals('amount', $missingAttributes[0]);
        }

        //Attempt to send a payment with an invalid (negative) amount
        $paymentFailed = false;
        try {
            $payment = new Payment;
            $payment->token = $retrievedCreditCard->id;
            $payment->reference = 'Test payment.';
            $payment->statement_descriptor = 'Statement descriptor';
            $payment->amount = -1;
            $processedPayment = $payment->submit();
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\InvalidAmountException", get_class($e));
            $this->assertEquals(400, $e->getCode());
            $paymentFailed = true;
        } finally {
            if (!$paymentFailed) {
                $this->fail("Payment should fail for invalid (negative) amount but it succeeded");
            }
        }

        //Attempt to send a payment with an invalid payment source
        $paymentFailed = false;
        try {
            $payment = new Payment;
            $payment->token = 'invalid';
            $payment->reference = 'Test payment.';
            $payment->statement_descriptor = 'Statement descriptor';
            $payment->amount = 100;
            $processedPayment = $payment->submit();
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\InvalidPaymentSourceException", get_class($e));
            $this->assertEquals(400, $e->getCode());
            $paymentFailed = true;
        } finally {
            if (!$paymentFailed) {
                $this->fail("Payment should fail for invalid payment source but it succeeded");
            }
        }

        //Attempt to send a payment with an invalid clearing account
        $paymentFailed = false;
        try {
            $payment = new Payment;
            $payment->token = $retrievedCreditCard->id;
            $payment->reference = 'Test payment.';
            $payment->statement_descriptor = 'Statement descriptor';
            $payment->amount = 100;
            $payment->clearing_account = 1234567890;
            $processedPayment = $payment->submit();
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\ResourceNotFoundException", get_class($e));
            $this->assertEquals(400, $e->getCode());
            $paymentFailed = true;
        } finally {
            if (!$paymentFailed) {
                $this->fail("Payment should fail for invalid clearing account but it succeeded");
            }
        }


        //Process a payment and retrieve the refund transaction
        $payment = new Payment;
        $payment->amount = 100;
        $payment->token = $retrievedCreditCard->id;
        $payment->reference = 'Test payment.';
        $payment->statement_descriptor = 'Statement descriptor';
        $processedPayment = $payment->submit();
        $retrievedPayment = Payment::retrieve($processedPayment->txn_id);
        $processedRefund = $retrievedPayment->refund(Payment::REFUND_RETURN_OBJECT);
        $this->assertEquals($processedPayment->txn_id, $processedRefund->original_txn_id);
        $this->assertEquals($processedPayment->amount, $processedRefund->amount);

        //Delete the customer
        $retrievedCustomer->delete();
    }

    public function testPaymentExceptionHandling()
    {
        EoneoPay::setHttpErrors(true);

        $paymentFailed = false;
        try {
            $payment = new Payment;
            $payment->amount = 100;
            $payment->token = 'invalid-token';
            $payment->reference = 'Test payment.';
            $payment->statement_descriptor = 'Statement descriptor';
            $processedPayment = $payment->submit();
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\InvalidPaymentSourceException", get_class($e));
            $paymentFailed = true;
        } finally {
            if (!$paymentFailed) {
                $this->fail("Payment should fail for mismatched allocation amount but it succeeded");
            }
        }

    }

    public function testPaymentByTxnDate()
    {
        $iDeltaHours = 12;
        $sStart = date("Y-m-d H:i:s", strtotime("-$iDeltaHours hours"));
        $aPayments = Payment::all(["txn_date", ">=", $sStart]);
        $this->assertNotEmpty($aPayments);
        foreach($aPayments as $oPayment) {
            if ($oPayment->txn_date == null) {
                continue;
            }
            $iTxnDate = strtotime($oPayment->txn_date);
            $iDiffHours = (time() - $iTxnDate) / 3600;
            /*if ($iDiffHours > $iDeltaHours) {
                var_dump(date("Y-m-d H:i:s"), time());
                var_dump($sStart);
                var_dump($oPayment);
            }*/
            $this->assertLessThanOrEqual($iDeltaHours,$iDiffHours);
        }
    }

    public function testPaymentCrossMerchant()
    {
        $aPayments = Payment::all(["merchant_id" => $_ENV[$this->eoneoEnvironment . 'EONEOPAY_MERCHANT_ID']], 50);
        $lastDate = '';
        foreach ($aPayments as $oPayment) {
            $this->assertEquals($_ENV[$this->eoneoEnvironment . 'EONEOPAY_MERCHANT_ID'], $oPayment->merchant_id);
            $this->assertGreaterThanOrEqual($lastDate, $oPayment->txn_date);
            $lastDate = $oPayment->txn_date;
        }
    }

    public function testCrns()
    {
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $retrievedCustomer = $customer->save();

        //$bpayCrn = $retrievedCustomer->allocateBpayCrn();
        $bpayCrnDetails = $retrievedCustomer->allocateBpayCrn();
        $bpayCrn = $bpayCrnDetails->crn;
        $billerCode = $bpayCrnDetails->billerCode;

        $ausPostCrnDetails = $retrievedCustomer->allocateAusPostCrn();
        $ausPostCrn = $ausPostCrnDetails->ausPostCrn;
        $pipId = $ausPostCrnDetails->pipId;

        $eftIdDetails = $retrievedCustomer->allocateEftId();
        $eftId = $eftIdDetails->eftId;
        $bsb = $eftIdDetails->bsb;

        $this->assertNotNull($bpayCrn);
        $this->assertNotNull($billerCode);
        $this->assertNotNull($ausPostCrn);
        $this->assertNotNull($pipId);
        $this->assertNotNull($eftId);
        $this->assertNotNull($bsb);

        $billerCode = "";

        $customerWithCrns = Customer::retrieve($retrievedCustomer->id);
        //var_dump($customerWithCrns);
        $this->assertNotEmpty($customerWithCrns->reference_numbers);
        $this->assertTrue(is_array($customerWithCrns->reference_numbers));
        $this->assertEquals(5, sizeof($customerWithCrns->reference_numbers));
        $this->assertInstanceOf("EoneoPay\CustomerReferenceNumber", $customerWithCrns->reference_numbers[0]);
        $this->assertInstanceOf("EoneoPay\CustomerReferenceNumber", $customerWithCrns->reference_numbers[1]);
        $this->assertInstanceOf("EoneoPay\CustomerReferenceNumber", $customerWithCrns->reference_numbers[2]);
        foreach ($customerWithCrns->reference_numbers as $reference_number) {
            if ($reference_number->type == 'bpay') {
                $this->assertTrue($bpayCrn === $reference_number->reference_number || $ausPostCrn === $reference_number->reference_number);
                $this->assertNotEmpty($reference_number->payment_facility->biller_code);
                $billerCode = $reference_number->payment_facility->biller_code;
            } else if ($reference_number->type == 'auspost') {
                $this->assertTrue($bpayCrn === $reference_number->reference_number || $ausPostCrn === $reference_number->reference_number);
                $this->assertNotEmpty($reference_number->payment_facility->pip_id);
                $pipId = $reference_number->payment_facility->pip_id;
            } else if ($reference_number->type == 'allocated_eft') {
                $this->assertEquals($eftId, $reference_number->reference_number);
            } else {
                $this->fail("Customer reference number type not recognised: " . $reference_number->type);
            }
        }

        //Get the barcode and write it to file for manual validation,
        //no automated assertion here other than confirming there is no outright failure
        $barcode = $customerWithCrns->getAusPostBarcode($bpayCrn, $pipId, 100);
        $this->assertNotNull($barcode);

        $file = fopen("barcode.png", "w");
        fwrite($file, $barcode);
        fclose($file);

        //Get the BPAY bill advice and write it to file for manual validation,
        //no automated assertion here other than confirming there is no outright failure
        $billAdvice = $customerWithCrns->getBPAYBillAdvice($bpayCrn, $billerCode);
        $this->assertNotNull($billAdvice);

        $file = fopen("billAdvice.png", "w");
        fwrite($file, $billAdvice);
        fclose($file);

        //Get the horizontal BPAY bill advice and write it to file for manual validation,
        //no automated assertion here other than confirming there is no outright failure
        $billAdvice = $customerWithCrns->getBPAYBillAdvice($bpayCrn, $billerCode, true);
        $this->assertNotNull($billAdvice);

        $file = fopen("billAdvice-horizontal.png", "w");
        fwrite($file, $billAdvice);
        fclose($file);
    }

    public function testBalances()
    {
        $originalBalance = Balance::retrieve()->total->AUD->amount;

        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $retrievedCustomer = $customer->save();

        //Add a credit card
        $creditCard = new CreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';
        
        $retrievedCreditCard = $customer->addCreditCard($creditCard);
        
        //Create a new payment of $1, note the amount is in cents
        //Add the credit card token to the payment
        $payment = new Payment;
        $payment->amount = 100;
        $payment->token = $retrievedCreditCard->id;
        $payment->reference = 'Test payment.';
        $processedPayment = $payment->submit();

        $this->assertEquals($originalBalance + ($processedPayment->amount - $processedPayment->fee_amount), Balance::retrieve()->total->AUD->amount);
        //$this->assertEquals($originalBalance + 68, Balance::retrieve()->total->AUD->amount);

        $history = Balance::history(["from" => date("Y-m-d"), 'transfer_status' => 0]);
        $found = false;
        foreach ($history->payments as $item) {
            if ($item->txn_id == $processedPayment->txn_id) {
                $this->assertEquals(100, $item->amount);
                $this->assertEquals("cc_payment", $item->type);
                $found = true;
            }
        }
        $this->assertTrue($found);

        $refunded = $processedPayment->refund();
        $this->assertTrue($refunded);
        $this->assertEquals($originalBalance, Balance::retrieve()->total->AUD->amount);

        //Cannot delete default payment sources
        //$retrievedCreditCard->delete();
        $retrievedCustomer->delete();
    }

    public function testTransfers()
    {
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $retrievedCustomer = $customer->save();

        //Add a credit card
        $creditCard = new CreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';
        
        $retrievedCreditCard = $customer->addCreditCard($creditCard);
        
        //Create a new payment of $1, note the amount is in cents
        //Add the credit card token to the payment
        $payment = new Payment;
        $payment->amount = 100;
        $payment->token = $retrievedCreditCard->id;
        $payment->reference = 'Test payment.';
        $processedPayment = $payment->submit();

        $balance = Balance::retrieve()->total->AUD->amount;

        $transfer = new Transfer;
        $transfer->destination = 'src_pD7tKjMlY28Ho3BT';
        $transfer->amount = $balance;
        $transfer->description = 'Test transfer';
        $transfer->currency = 'AUD';

        try {
            $failedTransfer = $transfer->submit();
            $this->fail("Transfer request does not include transaction list. The transfer should fail but it has succeeded.");
        } catch (Exception $e) {
            //Excpected. There should be an exception because the transfer does not include a transaction list
        }

        //From 0.2.0 the current balance cannot be transferred, only the available balance can be transferred
        /*$transfer->amount = $balance;
        $processedTransfer = $transfer->submit();
        $this->assertEquals($processedTransfer->amount, $transfer->amount);

        $balance = Balance::retrieve()->total->AUD->amount;
        $this->assertEquals(0, $balance);*/

        $history = Balance::history(["from" => date("Y-m-d")]);
        $found = false;
        foreach ($history->payments as $item) {
            if ($item->txn_id == $processedPayment->txn_id) {
                $this->assertEquals(100, $item->amount);
                $this->assertEquals("cc_payment", $item->type);
                $found = true;
            }
        }
        $this->assertTrue($found);

        /*$found = false;
        foreach ($history->transfers as $item) {
            if ($item->id == $processedTransfer->id) {
                $this->assertEquals($transfer->amount, $item->amount);
                $found = true;
            }
        }
        $this->assertTrue($found);*/
    }

    public function testPlansAndSubscriptions()
    {
        $plan = new Plan;
        $plan->name = 'Test Plan';
        $plan->amount = 100;
        $plan->interval = Plan::INTERVAL_MONTH;
        $plan->interval_count = 2;

        $fee = new PlanFee;
        $fee->description = 'Test Plan Fee';
        $fee->amount = 100;
        $fee->trigger = PlanFee::TRIGGER_PERIODICAL;
        $fee->proration = PlanFee::PRORATION_ALL;

        $plan->addFee($fee);

        $retrievedPlan = $plan->save();

        $this->assertEquals($retrievedPlan->name, 'Test Plan');
        $this->assertEquals($retrievedPlan->interval, Plan::INTERVAL_MONTH);
        $this->assertEquals($retrievedPlan->amount, 100);
        $this->assertEquals($retrievedPlan->interval_count, 2);

        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_subscriptions@example.com';
        $customer->external_customer_id = 'Test_'.time();
        $retrievedCustomer = $customer->save();

        $bpayCrnDetails = $retrievedCustomer->allocateBpayCrn();

        $subscription = new Subscription;
        $subscription->plan = $plan->id;
        $subscription->start_date = date("Y-m-d");
        $subscription->end_date = strtotime("+1 year", time());
        $subscription->bpay_reference_number = $bpayCrnDetails->crn;
        $newSubscription = $retrievedCustomer->addSubscription($subscription);

        $retrievedSubscription = Subscription::retrieve($newSubscription->id);
        $this->assertEquals($retrievedSubscription->id, $newSubscription->id);
        $this->assertEquals($retrievedSubscription->plan, $newSubscription->plan);
        $this->assertNotEmpty($retrievedSubscription->payment_facilities);
        $this->assertNotEmpty($retrievedSubscription->payment_facilities->bpay);
        $this->assertEquals('bpay', $retrievedSubscription->payment_facilities->bpay->type);

        //Create a new credit card
        $creditCard = new CreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';

        //Add the credit card to the customer, calls the API to store the card and obtain a token
        $customer->addCreditCard($creditCard);

        $retrievedSubscription->source = $creditCard->id;
        $retrievedSubscription->save();

        $payment = new Payment;
        $payment->amount = 100;
        $payment->token = $creditCard->id;
        $payment->reference = 'Test payment.';
        $payment->reference_number = $subscription->id;
        $processedPayment = $payment->submit();

        $paymentList = $subscription->getPayments();
        $this->assertNotEmpty($paymentList);
        $this->assertTrue(is_array($paymentList));
        $this->assertEquals(1, sizeof($paymentList));
        $this->assertEquals($payment->txn_id, $paymentList[0]->txn_id);

        $subscriptionList = Subscription::getList($retrievedCustomer->external_customer_id);
        $this->assertEquals(1, sizeof($subscriptionList));
        $this->assertEquals($subscriptionList[0]->id, $subscription->id);

        //var_dump(Payment::all(["reference_number", $subscription->id]));
        //$retrievedSubscription->cancel();
        //$retrievedPlan->delete();
    }

    public function testOverdueSubscriptions()
    {
        $plan = new Plan;
        $plan->name = 'Test Plan';
        $plan->amount = 100;
        $plan->interval = Plan::INTERVAL_MONTH;
        $plan->interval_count = 1;

        $retrievedPlan = $plan->save();

        $customer = new Customer;
        $customer->first_name = 'Overdue';
        $customer->last_name = 'Subscription';
        $customer->email = 'test_overdue_subscriptions@example.com';
        $retrievedCustomer = $customer->save();

        $subscriptions = [];

        $subscription = new Subscription;
        $subscription->plan = $plan->id;
        $subscription->start_date = date("Y-m-d", strtotime("-3 months"));
        $subscription->end_date = strtotime("+9 months");
        $newSubscription = $retrievedCustomer->addSubscription($subscription);

        $subscriptions[] = $newSubscription;

        $customer = new Customer;
        $customer->first_name = 'Overdue';
        $customer->last_name = 'Subscription2';
        $customer->email = 'test_overdue_subscriptions2@example.com';
        $retrievedCustomer = $customer->save();

        $subscription = new Subscription;
        $subscription->plan = $plan->id;
        $subscription->start_date = date("Y-m-d");
        $subscription->end_date = strtotime("+1 year");
        $newSubscription = $retrievedCustomer->addSubscription($subscription);

        $subscriptions[] = $newSubscription;

        $offset = 0;
        do {
            $overdueSubscriptions = Subscription::getOverdue(false, 100, $offset++);

            $foundSubscription = false;
            foreach ($overdueSubscriptions as $overdueSubscription) {
                if ($overdueSubscription->id == $subscriptions[0]->id) {
                    $foundSubscription = Subscription::retrieve($overdueSubscription->id);
                }
                $this->assertNotEquals($subscriptions[1]->id, $overdueSubscription->id);
            }
        } while ($foundSubscription === false && count($overdueSubscriptions) > 0);
        $this->assertNotEquals(0, count($overdueSubscriptions));
        $this->assertTrue($foundSubscription !== false);
        $this->assertEquals(300, $foundSubscription->statement->total_outstanding_to_date);
    }

    public function testPaymentAllocationPlan()
    {
        $plan = new Plan;
        $plan->name = 'Test Plan';
        $plan->amount = 10000;
        $plan->interval = Plan::INTERVAL_MONTH;
        $plan->interval_count = 1;

        $retrievedPlan = $plan->save();

        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_subscriptions@example.com';
        $retrievedCustomer = $customer->save();

        $subscription = new Subscription;
        $subscription->plan = $plan->id;
        $subscription->start_date = date("Y-m-d");
        $subscription->end_date = strtotime("+1 year", time());
        $newSubscription = $retrievedCustomer->addSubscription($subscription);

        $retrievedSubscription = Subscription::retrieve($newSubscription->id);

        //Create a new credit card
        $creditCard = new CreditCard;
        $creditCard->number = '4444333322221111';
        $creditCard->expiry_month = 12;
        $creditCard->expiry_year = 17;
        $creditCard->name = 'Mr Example';
        $creditCard->cvc = '123';

        //Add the credit card to the customer, calls the API to store the card and obtain a token
        $customer->addCreditCard($creditCard);

        $retrievedSubscription->source = $creditCard->id;
        $retrievedSubscription->save();

        $charge = new Charge();
        $charge->name = 'Test Charge';
        $charge->amount = 1000;
        $charge->save();

        $retrievedCharge = Charge::retrieve($charge->id);

        //Total amount calculated assuming a Visa card fee of 1.5% and a transaction fee of 30c
        $this->assertEquals($retrievedCharge->id, $charge->id);
        $payment = new Payment;
        $payment->amount = 11165;
        $payment->token = $creditCard->id;
        $payment->reference = 'Test payment.';
        $payment->allocation_plan = [
            $retrievedSubscription->id => [
                "type" => "subscription",
                "amount" => 10000,
            ],
            $retrievedCharge->id => [
                "type" => "charge",
                "amount" => 1000,
            ],
        ];
        $processedPayment = $payment->submit();

        $retrievedSubscription = Subscription::retrieve($retrievedSubscription->id);
        //var_dump($retrievedSubscription);
        $this->assertTrue(isset($retrievedSubscription->statement));
        $this->assertTrue(isset($retrievedSubscription->statement->paymentAllocations));
        $this->assertTrue(is_array($retrievedSubscription->statement->paymentAllocations));
        $this->assertEquals(1, count($retrievedSubscription->statement->paymentAllocations));
        $this->assertEquals($processedPayment->txn_id, $retrievedSubscription->statement->paymentAllocations[0]->txn_id);
        $this->assertEquals(10000, $retrievedSubscription->statement->paymentAllocations[0]->amount);

        $retrievedCharge = Charge::retrieve($retrievedCharge->id);
        $this->assertTrue(isset($retrievedCharge->statement));
        $this->assertTrue(isset($retrievedCharge->statement->paymentAllocations));
        $this->assertTrue(is_array($retrievedCharge->statement->paymentAllocations));
        $this->assertEquals(1, count($retrievedCharge->statement->paymentAllocations));
        $this->assertEquals($processedPayment->txn_id, $retrievedCharge->statement->paymentAllocations[0]->txn_id);
        $this->assertEquals(1000, $retrievedCharge->statement->paymentAllocations[0]->amount);

        //Attempt to send a payment with an mismatched allocation amount
        $paymentFailed = false;
        try {
            $payment = new Payment;
            $payment->amount = 11164;
            $payment->token = $creditCard->id;
            $payment->reference = 'Test payment.';
            $payment->allocation_plan = [
                $retrievedSubscription->id => [
                    "type" => "subscription",
                    "amount" => 10000,
                ],
                $retrievedCharge->id => [
                    "type" => "charge",
                    "amount" => 1000,
                ],
            ];
            $processedPayment = $payment->submit();
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\AllocationAmountMismatchException", get_class($e));
            $this->assertEquals(400, $e->getCode());
            $payment = $e->getPayment();
            $this->assertNotEmpty($payment);
            $this->assertEquals(11164, $payment->amount);
            $paymentFailed = true;
        } finally {
            if (!$paymentFailed) {
                $this->fail("Payment should fail for mismatched allocation amount but it succeeded");
            }
        }
        
        //Attempt to send a payment with a reference number not included in the allocation plan
        $paymentFailed = false;
        try {
            $payment = new Payment;
            $payment->amount = 11165;
            $payment->token = $creditCard->id;
            $payment->reference = 'Test payment.';
            $payment->reference_number = 'missing reference number';
            $payment->allocation_plan = [
                $retrievedSubscription->id => [
                    "type" => "subscription",
                    "amount" => 10000,
                ],
                $retrievedCharge->id => [
                    "type" => "charge",
                    "amount" => 1000,
                ],
            ];
            $processedPayment = $payment->submit();
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\AllocationMissingReferenceException", get_class($e));
            $this->assertEquals(400, $e->getCode());
            $paymentFailed = true;
        } finally {
            if (!$paymentFailed) {
                $this->fail("Payment should fail for missing reference number but it succeeded");
            }
        }

        $retrievedCharge->delete();
        $retrievedSubscription->cancel();
        $retrievedPlan->delete();
    }

    public function testCharges()
    {
        $charge = new Charge();
        $charge->name = 'Test Charge';
        $charge->amount = 100;
        $charge->save();

        $charge->due_date = '2016-05-01';
        $charge->save();

        $retrievedCharge = Charge::retrieve($charge->id);
        $this->assertEquals($retrievedCharge->id, $charge->id);
        $this->assertEquals($retrievedCharge->name, $charge->name);
        $this->assertEquals($retrievedCharge->amount, $charge->amount);
        $this->assertEquals($retrievedCharge->due_date, $charge->due_date);

        //test charge on subscription
        $plan = new Plan;
        $plan->name = 'Test Plan';
        $plan->amount = 100;
        $plan->interval = Plan::INTERVAL_MONTH;
        $plan->interval_count = 1;

        $retrievedPlan = $plan->save();

        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_subscriptions@example.com';
        $retrievedCustomer = $customer->save();

        $subscription = new Subscription;
        $subscription->plan = $plan->id;
        $subscription->start_date = date("Y-m-d");
        $subscription->end_date = strtotime("+1 year", time());
        $newSubscription = $retrievedCustomer->addSubscription($subscription);

        $retrievedSubscription = Subscription::retrieve($newSubscription->id);

        $retrievedSubscription->addCharge($charge);
    }

    public function testEWallet()
    {
        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_subscriptions@example.com';
        $retrievedCustomer = $customer->save();
        $this->assertNotEmpty($retrievedCustomer->ewallet_id);

        $ewallet = $retrievedCustomer->getEWallet();

        $this->assertEquals($retrievedCustomer->ewallet_id, $ewallet->id);
    }


    /*public function testAusPostForPavel()
    {
        //sk_live_hlPjiv71et7RuGjFK5II5VC8
        EoneoPay::setApiKey("sk_live_hlPjiv71et7RuGjFK5II5VC8");

        $customer = Customer::retrieve("cus_FRNOnaIa7OF6vAEZ");
        foreach ($customer->reference_numbers as $reference_number) {
            if ($reference_number->type == 'bpay') {
                $barcode = $customer->getAusPostBarcode($reference_number->reference_number, 500);
                $file = fopen("barcode.png", "w");
                fwrite($file, $barcode);
                fclose($file);
            }
        }
    }*/

    /*public function testBatchPayments()
    {
        //Increase the timeout here because batch payments can take some time to process
        EoneoPay::setTimeout(300.0);

        $customer = new Customer;
        $customer->first_name = 'First';
        $customer->last_name = 'Last';
        $customer->email = 'test_sources@example.com';
        $retrievedCustomer = $customer->save();

        $bpayCrnDetails = $retrievedCustomer->allocateBpayCrn();
        $bpayCrn = $bpayCrnDetails->crn;

        $testBatchPayment = new TestBatchPayment();
        $testBatchPayment->amount = 22040;
        $testBatchPayment->description = "Test BPAY Batch " . rand(0, 99999);
        $testBatchPayment->type = TestBatchPayment::BATCH_TYPE_BPAY;
        $testBatchPayment->crn = $bpayCrn;

        $batchPayment = $testBatchPayment->submit();

        $payments = Payment::all(["reference_number" => $bpayCrn, "type" => TestBatchPayment::BATCH_TYPE_BPAY]);
        $this->assertNotEmpty($payments);
        $this->assertTrue(is_array($payments));
        $this->assertEquals(1, count($payments));
        $this->assertInstanceOf('\EoneoPay\Payment', $payments[0]);
        $this->assertEquals($bpayCrn, $payments[0]->reference_number);
        $this->assertEquals('bpay', $payments[0]->type);
        $this->assertEquals($testBatchPayment->amount, $payments[0]->amount);

        $eftIdDetails = $retrievedCustomer->allocateEftId();
        $eftId = $eftIdDetails->eftId;

        $testBatchPayment = new TestBatchPayment();
        $testBatchPayment->amount = 33060;
        $testBatchPayment->description = "Test EFT Batch " . rand(0, 99999);
        $testBatchPayment->type = TestBatchPayment::BATCH_TYPE_ALLOCATED_EFT;
        $testBatchPayment->eft_id = $eftId;

        $batchPayment = $testBatchPayment->submit();

        $payments = Payment::all(["reference_number" => $eftId, "type" => TestBatchPayment::BATCH_TYPE_ALLOCATED_EFT]);
        $this->assertNotEmpty($payments);
        $this->assertTrue(is_array($payments));
        $this->assertEquals(1, count($payments));
        $this->assertInstanceOf('\EoneoPay\Payment', $payments[0]);
        $this->assertEquals($eftId, $payments[0]->reference_number);
        $this->assertEquals('eft', $payments[0]->type);
        $this->assertEquals($testBatchPayment->amount, $payments[0]->amount);

        $ausPostCrnDetails = $retrievedCustomer->allocateAusPostCrn();
        $ausPostCrn = $ausPostCrnDetails->ausPostCrn;

        $testBatchPayment = new TestBatchPayment();
        $testBatchPayment->amount = 44080;
        $testBatchPayment->description = "Test AusPost Batch " . rand(0, 99999);
        $testBatchPayment->type = TestBatchPayment::BATCH_TYPE_AUSPOST;
        $testBatchPayment->crn = $ausPostCrn;

        $batchPayment = $testBatchPayment->submit();

        $payments = Payment::all(["reference_number" => $ausPostCrn, "type" => TestBatchPayment::BATCH_TYPE_AUSPOST]);
        $this->assertNotEmpty($payments);
        $this->assertTrue(is_array($payments));
        $this->assertEquals(1, count($payments));
        $this->assertInstanceOf('\EoneoPay\Payment', $payments[0]);
        $this->assertEquals($bpayCrn, $payments[0]->reference_number);
        $this->assertEquals('auspost', $payments[0]->type);
        $this->assertEquals($testBatchPayment->amount, $payments[0]->amount);
    }*/

    public function testConnectionException()
    {
        EoneoPay::setBaseUri('someinvalidurl');
        try {
            $customers = Customer::all();
            $this->fail("Expected Guzzle connection exception");
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\EoneoException", get_class($e));
        }
    }
}
