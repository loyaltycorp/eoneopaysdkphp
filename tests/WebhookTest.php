<?php

require __DIR__ . "/../vendor/autoload.php";

use EoneoPay\EoneoPay;
use EoneoPay\Webhook;

class WebhookTest extends TestCase
{
    public function testCanUserCreateAndListWebhooks()
    {
        $webhook = new Webhook;
        $webhook->name = 'First';
        $webhook->target_url = 'http://localhost';
        $webhook->event_name = 'PaymentCreated';
        $webhook->active = true;
        $webhook->force_url = true;
        $webhook->merchant_id = $_ENV[$this->eoneoEnvironment . 'EONEOPAY_MERCHANT_ID'];
        $retrievedWebhook = $webhook->save();

        $this->assertEquals($webhook->name, $retrievedWebhook->name);

        $retrievedWebhooks = Webhook::all([], 100, 0);
    }

    public function testCanUserRetrieveWebhook()
    {
        $webhook = new Webhook;
        $webhook->name = 'First';
        $webhook->target_url = 'http://localhost';
        $webhook->event_name = 'PaymentCreated';
        $webhook->active = true;
        $webhook->force_url = true;
        $webhook->merchant_id = $_ENV[$this->eoneoEnvironment . 'EONEOPAY_MERCHANT_ID'];
        $retrievedWebhook = $webhook->save();
        $retrievedWebhookId = $retrievedWebhook->id;

        $webhook = Webhook::retrieve($retrievedWebhookId);   
    }

    public function testCanUserDeleteWebhooks()
    {
        $webhook = new Webhook;
        $webhook->name = 'First';
        $webhook->target_url = 'http://localhost';
        $webhook->event_name = 'PaymentCreated';
        $webhook->active = true;
        $webhook->force_url = true;
        $webhook->merchant_id = $_ENV[$this->eoneoEnvironment . 'EONEOPAY_MERCHANT_ID'];
        $retrievedWebhook = $webhook->save();
        $retrievedWebhookId = $retrievedWebhook->id;

        $response = $retrievedWebhook->delete();
        $retrieveFailed = false;
        try {
            $webhook = Webhook::retrieve($retrievedWebhookId);    
        } catch (\Exception $e) {
            $this->assertEquals("EoneoPay\Exception\ResourceNotFoundException", get_class($e));
            $this->assertEquals(404, $e->getCode());
            $retrieveFailed = true;
        } finally {
            if (!$retrieveFailed) {
                $this->fail("Retrieve should have failed cause webhook has been deleted");
            }
        }
    }
}

