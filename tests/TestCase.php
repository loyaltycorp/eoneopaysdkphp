<?php
use EoneoPay\EoneoPay;

class TestCase extends PHPUnit_Framework_TestCase
{
    public $eoneoEnvironment;

    /**
     * Set up the API end point and key for each test.
     */
    public function setup()
    {
        //Eoneo environment
        $this->eoneoEnvironment = 'DEV';
        if (!empty($_ENV['EONEOPAY_ENV'])) {
            $this->eoneoEnvironment = $_ENV['EONEOPAY_ENV'];
        }
        $this->eoneoEnvironment .= '_';

        //Set the API end point
        EoneoPay::setBaseUri($_ENV[$this->eoneoEnvironment . 'EONEOPAY_ENDPOINT']);

        //Set the merchant API key
        EoneoPay::setApiKey($_ENV[$this->eoneoEnvironment . 'EONEOPAY_API_KEY']);

        //Set API timeout
        EoneoPay::setTimeout(30.0);

        EoneoPay::setEoneoExceptions(true);
    }
}
