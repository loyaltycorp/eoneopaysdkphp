## EoneoPay Payment Gateway API Client

Eoneo payment gateway API Client.

## Documentation

# Setup
1. Composer

Add the following entries to composer.json:
```
"repositories": [
    {
        "type": "vcs",
        "url": "https://git@bitbucket.org/loyaltycorp/eoneopayclientlibrary_php.git"
    }
],
"require": {
    "eoneopay/client": "dev-master"
}
```

Run composer install
```shell
composer install
```

2. Use the EoneoPay API client:
```php
use EoneoPay\EoneoPay;
```

3. The library assumes the base API URI is https://api.eoneopay.com. To set a different base URI:
```php
EoneoPay::setBaseUri('https://api.eoneopay.com');
```

4. Set the API key
```php
EoneoPay::setApiKey('sk_test_someapikey');
```

5. Choose exception handling method
```php
//By default the SDK throws all Guzzle HTTP exceptions
//To use internal Eoneo SDK exception call the following:
EoneoPay::setEoneoExceptions(true);

//Guzzle exceptions can also be turned off. In this mode responses would have to be examined for status codes and error messages.
//Note that Eoneo SDK errors cannot be enabled if HTTP errors are disabled.
EoneoPay::setHttpErrors(false);

```

# Create a customer
To create customer instantiate a Customer, set properties and save:
```php
//use EoneoPay\Customer;
$customer = new Customer;
//required
$customer->first_name = 'First Name';
$customer->last_name = 'Last Name';

//optional
$customer->email = 'test1@example.com';

//optional external ID which can be used to retrieve the customer
$customer->external_customer_id = '12345';

$customer = $customer->save();
```

# Retrieve a customer
Get a valid customer ID and call Customer::retrieve()
```php
$id = 'cus_1234567890';
$retrievedCustomer = Customer::retrieve($id);

//Or retrieve by external id
$retrievedCustomer = Customer::retrieve($external_customer_id);

//Get the customer ewallet
$ewallet = $retrievedCustomer->getEWallet();
```

# Update a customer
Retrieve a customer, change properties and call Customer::save()
```php
$id = 'cus_1234567890';
$customer = Customer::retrieve($id);
//required
$customer->first_name = 'First Name';
$customer->last_name = 'Last Name';

//Optional
$customer->email = 'test3@example.com';

$customer = $customer->save();
```

# Delete a customer
Retrieve a customer through the API and then call Customer::delete()
```php
$customer->delete();
```
# List customers
Get an array of customers with an optional filter.
```php
$listCustomers = Customer::all([], 10, 1); //Get customers 11-20, i.e. offset 1 with pages of 10 customers, offset 0 being the first page
$customersWithLastNameSmith = Customer::all(['last_name' => 'Smith']); //default limit is 100
```

# Add a credit card
Get a customer and add a credit card
```php
//Valid test credit cards:
//  Visa: 4444333322221111
//  MasterCard: 5431111111111111
$creditCard = new CreditCard;
$creditCard->number = '123456789012';
$creditCard->expiry_month = 12;
$creditCard->expiry_year = 17;
$creditCard->name = 'Mr Example';
$creditCard->cvc = '123';

$id = 'cus_1234567890';
$customer = Customer::retrieve($id);
$addedCreditCard = $customer->addCreditCard($creditCard);

//Set this card as the default
$addedCreditCard->setDefault();

//To remove the credit card, note that the default card cannot be removed
if (!$addedCreditCard->isDefault()) {
    $addedCreditCard->delete();
}
```

# Credit card tokens
Get a token for a credit card and add credit cards to a customer by token
```php
//Instantiate a CreditCard
$creditCard = new CreditCard;
$creditCard->number = '4444333322221111';
$creditCard->expiry_month = 12;
$creditCard->expiry_year = 17;
$creditCard->name = 'Mr Example';
$creditCard->cvc = '123';

//Create a new token
$token = new Token;
$token->card = $creditCard;
$savedToken = $token->save();

$retrievedToken = Token::retrieve($savedToken->id);

//Make a payment using the token
$payment = new Payment;
$payment->amount = 100;
$payment->token = $savedToken->id;
$payment->reference = 'Test payment.';
$processedPayment = $payment->submit();

//Adding cards to a customer using a token
//Find a customer to attach the card to
$id = 'cus_1234567890';
$customer = Customer::retrieve($id);

//Instantiate a CreditCard with a token only
$tokenCard = new CreditCard;
$tokenCard->token = $retrievedToken->id;

//Attach the credit card to the customer by token
$retrievedCreditCard = $customer->addCreditCard($tokenCard);
```

# Add a bank account
Get a customer and add a bank account
```php
$bankAccount = new BankAccount();
$bankAccount->number = '012345';
$bankAccount->bsb = '123-456';
$bankAccount->name = 'Mr Example Account';

$id = 'cus_1234567890';
$customer = Customer::retrieve($id);
$addedBankAccount = $customer->addBankAccount($bankAccount);

//To remove the bank account
$addedBankAccount->delete();
```

# Create a payment
Get a credit card from a customer and attach it to a payment
```php
$payment = new Payment;
$payment->amount = 100;
$payment->token = $creditCard->id;
$payment->reference = 'Test payment.';
$processedPayment = $payment->save();

//Note: When testing, to force the test payment processor to mimic the NAB payment validation process set the 
//  test_validate_amount flag on the payment (see below). This causes the payment to fail for any value that
//  does not end in 00, 08, 11 or 16.
//From NAB XML API integration guide: 
//  If the payment amount ends in 00, 08, 11 or 16, the transaction will be approved once card details are submitted. 
//  All other options will cause a declined transaction.
$payment->test_validate_amount = true;

//To create a payment with an allocation plan
$payment = new Payment;
$payment->amount = 6000;
$payment->token = $creditCard->id;
$payment->reference = 'Test payment.';
$payment->allocation_plan = [
    $retrievedSubscription->id => [
        "type" => "subscription",
        "amount" => 4000,
    ],
    $retrievedCharge->id => [
        "type" => "charge",
        "amount" => 1000,
    ],
];
$processedPayment = $payment->submit();

//To refund a payment
$refunded = $processedPayment->refund();
if (!$refunded) {
    echo 'Refund failed';
}
//By default the refund method returns a boolean result, true for success, false otherwise
//To get the refund transaction returned call:
//$refundTransaction = $processedPayment->refund(Payment::REFUND_RETURN_OBJECT);
```

# Create a plan
Use plans for subscriptions and payment schedules. After creating a plan a Customer Subscription can be created against the plan.
```php
$plan = new Plan;
$plan->name = 'Test Plan';
//The payment amount in cents
$plan->amount = 100;
//The internals are INTERVAL_DAY, INTERVAL_WEEK, INTERVAL_MONTH and INTERVAL_YEAR
$plan->interval = Plan::INTERVAL_MONTH;
//How many intervals between each payment, e.g. INTERVAL_WEEK with interval_count = 2 means a payment is made every 2 weeks.
$plan->interval_count = 1;

//One or more fees can optionally be added to a plan
$fee = new PlanFee;
$fee->description = 'Test Plan Fee';
//The fee amount in cents
$fee->amount = 100;
//The trigger for a fee payment, one of TRIGGER_PERIODICAL, TRIGGER_TRANSACTIONAL, TRIGGER_TERMINAL or TRIGGER_INITIAL. See comments in src/PlanFee.php for details.
$fee->trigger = PlanFee::TRIGGER_PERIODICAL;
//For TRIGGER_PERIODICAL payments the prorata rule is one of PRORATION_ALL, PRORATION_FIRST or PRORATION_LAST. See comments in src/PlanFee.php for details.
$fee->proration = PlanFee::PRORATION_ALL;

$plan->addFee($fee);

$retrievedPlan = $plan->save();

//To delete a plan
$retrievedPlan->delete();
```

# Create a subscription
Customers must subscribe to plans.
```php
Customer = new Customer;
$customer->email = 'test_subscriptions@example.com';
$retrievedCustomer = $customer->save();

$subscription = new Subscription;
$subscription->plan = $plan->id;

//Start date is optional but is required for balance calculations
$subscription->start_date = date("Y-m-d");

//End date is optional but is required for terminal fee triggers (see TRIGGER_TERMINAL for the fee creation above)
$subscription->end_date = strtotime("=1 year", time());

//If a customer will be paying the subscription by BPAY, AusPost or Allocated EFT the CRN must be added to the subscription to facilitate accurate payment allocations.
//$subscription->payment_reference = 'some_crn';
$newSubscription = $retrievedCustomer->addSubscription($subscription);

$retrievedSubscription = Subscription::retrieve($newSubscription->id);

//Create a subscription payment
$payment = new Payment;
$payment->reference_number = $retrievedSubscription->id;
//Amount in cents
$payment->amount = 100;
$payment->reference = 'Some descriptive text';
$payment->token = 'tok_sometoken';
$payment->submit();

//Add a charge to a subscription
$charge = new Charge();
$charge->name = 'Test Charge';
$charge->amount = 100;
$charge->save();
$retrievedSubscription->addCharge($charge);

//Get the current subscription statement showing the outstanding balance, payments and payment allocations
//The statement show the outstanding balance for the period and the total_oustanding_balance_to_date which is an accummulated total oustanding balance to the current date
$retrievedSubscription->getStatement();

//Get a statement for a specific billing period
$retrievedSubscription->getStatement("2016-01-01");

//To get a list of subscriptions for a customer
$subscriptionList = Subscription::getList($customer->id); //returns an array of EoneoPay\Subscription

//To cancel a subscription
$retrievedSubscription->cancel();
```

# BPay CRN, Allocated EFT ID and AusPost Barcode
```php
$customer = new Customer;
$customer->first_name = 'First';
$customer->last_name = 'Last';
$customer->email = 'test_sources@example.com';
$retrievedCustomer = $customer->save();

$bpayCrnDetails = $retrievedCustomer->allocateBpayCrn();
$bpayCrn = $bpayCrnDetails->crn;
$billerCode = $bpayCrnDetails->billerCode;

$ausPostCrnDetails = $retrievedCustomer->allocateAusPostCrn();
$ausPostCrn = $ausPostCrnDetails->crn;
$pipId = $ausPostCrnDetails->pipId;

$eftIdDetails = $retrievedCustomer->allocateEftId();
$eftId = $eftIdDetails->eftId;
$bsb = $eftIdDetails->bsb;

$customerWithCrns = Customer::retrieve($retrievedCustomer->id);
foreach ($customerWithCrns->reference_numbers as $reference_number) {
    if ($reference_number->type == 'bpay') {
        $bpayCrn = $reference_number->reference_number;
    } else if ($reference_number->type == 'allocated_eft') {
        $eftId = $reference_number->reference_number;
    }
}

//Use BPay CRN and amount to get an AusPost barcode
//Amount is in cents
$barcode = $customerWithCrns->getAusPostBarcode($bpayCrn, $pipId, 100);
if ($barcode) {
    $file = fopen("barcode.png", "w");
    fwrite($file, $barcode);
    fclose($file);
}

//Use BPay CRN and BillerCode to get a BPay Bill Advice
//For the horizontal bill advice call Customer::getBPAYBillAdvice($bpayCrn, $billerCode, true);
$billAdvice = $customerWithCrns->getBPAYBillAdvice($bpayCrn, $billerCode);
if ($billAdvice) {
    $file = fopen("billadvice.png", "w");
    fwrite($file, $billAdvice);
    fclose($file);
}
```
# Associate BPAY CRN wnd EFT ID with a subscription
```php
$customer = new Customer;
$customer->first_name = 'First';
$customer->last_name = 'Last';
$customer->email = 'test_sources@example.com';
$retrievedCustomer = $customer->save();

$bpayCrn = $retrievedCustomer->allocateBpayCrn();
$eftId = $retrievedCustomer->allocateEftId();


$subscription = new Subscription;
$subscription->plan = $plan->id;

//Start date is optional but is required for balance calculations
$subscription->start_date = date("Y-m-d");

//End date is optional but is required for terminal fee triggers (see TRIGGER_TERMINAL for the fee creation above)
$subscription->end_date = strtotime("+1 year", time());

//Lead time is the number of days prior to the due date payment should be processed for each period
//This applies to BPAY, AusPost and EFT transactions
$subscription->lead_time = 3 //payment will be processed 3 days prior to the due date

//If a customer will be paying the subscription by BPAY, AusPost or Allocated EFT the CRN must be added to the subscription to facilitate accurate payment allocations.
//$subscription->payment_reference = 'some_crn';
$newSubscription = $retrievedCustomer->addSubscription($subscription);

$newSubscription->bpay_reference_number = $bpayCrn;
$newSubscription->allocated_eft_reference_number = $eftId;
$newSubscription->save();

//Payment facilities provide access to biller codes and Pay in Person IDs
//$newSubscription->payment_facilities->bpay;
//$newSubscription->payment_facilities->allocated_eft
//$newSubscription->payment_facilities->auspost
```

#Setup balance transfers
Merchants can transfer funds from their available balance.

The available balance is updated daily. Transactions are transferred to the available balance after the settlement period has expired and the bank has confirm the payment is processed.

The transfer amount must match the total amount of one or more payment transactions and the specific transactions comprising the transfer amount must be attached to the tranfer request
in the transaction_list attribute.
```php
//Create a new payment of $1, note the amount is in cents
//Add the credit card token to the payment 
$payment = new Payment;
$payment->amount = 100;
$payment->token = $retrievedCreditCard->id;
$payment->reference = 'Test payment.';
$processedPayment = $payment->submit();

//Retrieve the available balance for the merchant
$balance = Balance::retrieve()->total->AUD->amount;

//Create a new transfer
$transfer = new Transfer;
$transfer->destination = 'src_pD7tKjMlY28Ho3BT';
$transfer->amount = $balance;
$transfer->description = 'Test transfer';
$transfer->currency = 'AUD';
$transfer->transaction_list = [$payment->txn_id];
$transfer->submit();
```

# Script to create customers with a plan and subscription
This is typical for tenancy agreements.
```shell
$ php scripts/CreateCustomerAndPlan.php 
Environment (1. Local, 2. Staging, 3 Live): 3
LIVE

Enter merchant API Key: ***************************
Authenticating...
Success.

Customer email: someone@somewhere.com
Customer first name: Some
Customer last name: One
Plan name: PLANNAME
Plan interval (1. Monthly, 2. Weekly): 1
Plan amount (cents): 125700
Add a plan fee (y/N)?n
Subscription start date (yyyymmdd): 20160916

BPAY CRN IS 7896615213
```

# Testing batch payments for BPAY, Allocated EFT and AusPost payments
```php
$customer = new Customer;
$customer->first_name = 'First';
$customer->last_name = 'Last';
$customer->email = 'test_sources@example.com';
$retrievedCustomer = $customer->save();

$bpayCrn = $retrievedCustomer->allocateBpayCrn();

//The timeout may need to be increased for testing batch payments
EoneoPay::setTimeout(300.0);

$testBatchPayment = new TestBatchPayment();
$testBatchPayment->amount = 22040;
$testBatchPayment->description = "Test BPAY Batch";
$testBatchPayment->type = TestBatchPayment::BATCH_TYPE_BPAY;
$testBatchPayment->crn = $bpayCrn;

$batchPayment = $testBatchPayment->submit();

$payments = Payment::all(["reference_number" => $bpayCrn, "type" => TestBatchPayment::BATCH_TYPE_BPAY]);

$eftId = $retrievedCustomer->allocateEftId();

$testBatchPayment = new TestBatchPayment();
$testBatchPayment->amount = 33060;
$testBatchPayment->description = "Test EFT Batch";
$testBatchPayment->type = TestBatchPayment::BATCH_TYPE_ALLOCATED_EFT;
$testBatchPayment->eft_id = $eftId;

$batchPayment = $testBatchPayment->submit();

$payments = Payment::all(["reference_number" => $eftId, "type" => TestBatchPayment::BATCH_TYPE_ALLOCATED_EFT]);

$testBatchPayment = new TestBatchPayment();
$testBatchPayment->amount = 44080;
$testBatchPayment->description = "Test AusPost Batch";
$testBatchPayment->type = TestBatchPayment::BATCH_TYPE_AUSPOST;
$testBatchPayment->crn = $bpayCrn;

$batchPayment = $testBatchPayment->submit();

$payments = Payment::all(["reference_number" => $bpayCrn, "type" => TestBatchPayment::BATCH_TYPE_AUSPOST]);
```

# Webhooks
For the following events a webhook can be created:
-PaymentCreated - Whenever a payment is created (succeeded or failed), this webhook is called
-PaymentFailed  - Whenever a payment has failed
-PaymentSucceeded - Whenever a payment has succeeded
-RefundSucceeded - Whenever a refund has succeeded
-RefundFailed - Whenever a refund has failed
-RefundCreated - Whenever a refund - failed or succeeded - has been created
-ReversalCreated - Whenever a reversal has been created
-PaymentMethodCreated - Whenever a payment method (credit card or bank account) is added to a customer
-PaymentMethodDeleted - Whenever a payment method (credit card or bank account) is deleted from a customer

A webhook can be created using the Webhook-class provided by this SDK.

```php
$webhook = new Webhook;
$webhook->name = 'First';
$webhook->target_url = 'http://localhost';
$webhook->event_name = 'PaymentCreated';
$webhook->active = true;
$webhook->force_url = true; //If this is not set or false, the URL must be valid and respond with a HTTP 200 to a POST
$webhook->merchant_id = $_ENV[$this->eoneoEnvironment . 'EONEOPAY_MERCHANT_ID'];
$retrievedWebhook = $webhook->save();

//Finding a webhook
$retrievedWebhook = Webhook::retrieve($webhook->id);

//Deleting a webhook
$webhook = Webhook::retrieve($retrievedWebhook->id);



# Masquerading
When authenticating as an Admin or a merchant with 'masquerade' permissions requests can be made on behalf of a second merchant.


```php
$customers = Customer::all(); //customers for authenticated merchant
EoneoPay::masqueradeAsMerchant('mer_1234567890123456');
$customers = Customer::all(); //customers for maqueraded merchant
```
